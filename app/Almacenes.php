<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Almacenes extends Model
{
    protected $fillable = ['id', 'nombre', 'codigo' ,'descripcion'];
    
    /**
	* Get data Tienda
	*/
	public function tienda()
	{
        return $this->belongsTo('App\Tienda','idTienda');
	}
   
}
