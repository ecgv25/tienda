<?php

namespace App\Exports;

use App\Obsequios;
use App\ProductosObsequios;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Exports\ProductosObsequiosExport;
use Maatwebsite\Excel\Concerns\Exportable;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

//class Export implements FromCollection
class ProductosObsequiosExport implements FromView
{
    public function view(): View
    {


        $idTienda = auth()->user()->idTienda;
        
        $productosObsequios = ProductosObsequios::where('idTienda', '=', $idTienda )->orderBy('id','DESC')->paginate(1000);
        $usuario = User::find(auth()->id());
        if($usuario->isAn('administrador')):
            $productosObsequios = ProductosObsequios::orderBy('id','DESC')->paginate(1000);

        endif; 
        return view('Obsequios.reporte',compact('productosObsequios')); 
        
        
       // return view('Ventas.reporte', [
        //    'ventas' => Ventas::all()
        //]);
    }
}
 