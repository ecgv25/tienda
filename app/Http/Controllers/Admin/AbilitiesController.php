<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StoreAbilitiesRequest;
use App\Http\Requests\Admin\UpdateAbilitiesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Silber\Bouncer\Database\Ability;

class AbilitiesController extends Controller
{
    protected $ability;

    public function __construct(Request $request)
    {
        if (! is_null($id = $request->ability)) {
            $this->ability = Ability::findOrFail(decrypt($id));
        }
   }

    /**
     * Display a listing of Abilities.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //if (! Gate::allows('habilidades')) {
       //     return abort(401);
       // }

        $abilities = Ability::all();

        return view('admin.abilities.index', compact('abilities'));
    }

    /**
     * Show the form for creating new Ability.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //if (! Gate::allows('habilidades')) {
       //     return abort(401);
       // }
        return view('admin.abilities.create');
    }

    /**
     * Store a newly created Ability in storage.
     *
     * @param  \App\Http\Requests\StoreAbilitiesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAbilitiesRequest $request)
    {
        //if (! Gate::allows('habilidades')) {
       //     return abort(401);
       // }

        $ability = Ability::create([
            'name' => str_slug(trim(strtolower($request->input('name'))), "_"),
        ]);

        return redirect()->route('admin_habilidades_index')->withStatus("Habilidad registrada con éxito");
    }


    /**
     * Show the form for editing Ability.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       // if (! Gate::allows('editar_habilidad')) {
        //    return abort(401);
      //  }
        $ability = Ability::find(decrypt($id));
        //$ability = $this->ability;

        return view('admin.abilities.edit', compact('ability'));
    }

    /**
     * Update Ability in storage.
     *
     * @param  \App\Http\Requests\UpdateAbilitiesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //if (! Gate::allows('editar_habilidad')) {
       //     return abort(401);
       // }
        $ability = Ability::find(decrypt($id));
        $ability->fill([
            'name' => str_slug(trim(strtolower($request->input('name'))), "_"),
            'title' => title_case($request->input('name')),
        ])->save();

        return redirect()->route('admin_habilidades_index')->withStatus("Habilidad actualizada con éxito");
    }


    /**
     * Remove Ability from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('eliminar_habilidad')) {
            return abort(401);
        }

        $this->ability->delete();

        return redirect()->route('admin.habilidades.index')->withStatus("Habilidad eliminada con éxito");
    }

    /**
     * Delete all selected Ability at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('eliminar_habilidad')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Ability::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
