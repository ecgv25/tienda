<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Traits\Auth\ChangesPasswords;

class PasswordsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password change Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password change for any
    | user that recently registered with the application.
    |
    */

    use ChangesPasswords;

    /**
     * Where to redirect users after changed password.
     *
     * @var string
     */
    protected $redirectTo = '/admin/inicio';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        //$this->middleware('signed')->only('change');
        //$this->middleware('throttle:6,1')->only('change', 'resend');
    }
}
