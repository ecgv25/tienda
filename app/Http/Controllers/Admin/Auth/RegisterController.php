<?php

namespace App\Http\Controllers\Admin\Auth;

use DB;
use App\Rules\Rif;
use App\Rules\Cedula;
use App\Rules\Telefono;
use App\Models\Estado;
use App\Models\Persona;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/inicio';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $estados = Estado::pluck('nombre', 'id')->prepend('Seleccione...', 0);
        return view('auth.register', compact('estados'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, $this->rules());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\Persona
     */
    protected function create(array $data)
    {
        $data = array_map('strtolower', array_except($data, ['_token']));

        $persona = Persona::create([
            'cedula' => $data['cedula'],
            'nombres' => $data['nombres'],
            'apellidos' => $data['apellidos'],
            'genero' => $data['genero'],
            'telefono_movil' => $data['telefono_movil'],
            'telefono_fijo' => $data['telefono_fijo'],
            'correo' => $data['correo'],
            'persona' => $data['persona'],
            'clave' => Hash::make(request()->password),
        ]);

        if ($data['persona'] === "juridica") {
            $persona->empresa()->create($data);
        }

        return $persona;
    }

    protected function rules()
    {
        $rules = [
            'cedula' => [
                'required',
                new Cedula,
                'unique:personas'
            ],
            'nombres' => 'required|string|max:191',
            'apellidos' => 'required|string|max:191',
            'genero' => 'required|in:m,f',
            'telefono_movil' => [
                'required',
                new Telefono,
                'unique:personas'
            ],
            'telefono_fijo' => [
                'required',
                new Telefono,
            ],
            'correo' => 'required|email|max:191|unique:personas',
            'persona' => 'required|in:juridica,natural',
            'password' => 'required|confirmed|min:8',
        ];

        if (request()->persona === "juridica") {
            $rules = $rules + [
                'rif' => [
                    'required',
                    new Rif,
                    'unique:empresas'
                ],
                'razon_social' => 'required|string|max:191',
                'id_estado' => 'required|exists:estados,id',
                'id_municipio' => 'required|exists:municipios,id',
                'id_parroquia' => 'required|exists:parroquias,id',
                'calle_avenida' => 'required|string|max:191',
                'sector_urbanizacion' => 'required|string|max:191',
                'casa_apto' => 'required|string|max:191',
                'telefono_fijo_empresa' => [
                    'required',
                    new Telefono,
                ],
                'correo_empresa' => 'required|email|max:191|unique:empresas,correo',
            ];
        }

        return $rules;
    }
}
