<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pago;
use App\Models\Persona;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'password.changed']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mineros = Persona::all()->count();
        $aprobados = Persona::whereNotNull('persona_verified_at')->count();
        $pagos = Pago::all()->count();
        $verificados = Pago::whereNotNull('verified_at')->count();
        return view('admin.home', compact('mineros','aprobados','pagos','verificados'));
    }
}
