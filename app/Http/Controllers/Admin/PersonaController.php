<?php

namespace App\Http\Controllers\Admin;

use ZipArchive;
use App\Models\Persona;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;

class PersonaController extends Controller
{
    protected $persona;

    protected $licencia;

    protected $pago;

    public function __construct(Request $request)
    {
        if (isset($request->persona)) {
            $this->persona = Persona::findOrFail(decrypt($request->persona));
        }

        if (isset($request->pago)) {
            $this->pago = $this->persona->pagos()->where('id', decrypt($request->pago))->first();
        }

        if (isset($request->licencia)) {
            $this->licencia = $this->persona->licencias()->where('id', decrypt($request->licencia))->first();
        }
    }
    
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mineros = Persona::filterAndPaginate(request()->only(['termino']));
        return view('admin.persona.index', compact('mineros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $persona = $this->persona;
        return view('admin.persona.show', compact('persona'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Download the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download($id)
    {
        $dir = storage_path('app/public/');
        $name = strtoupper($this->persona->cedula).".zip";
        $zip = new ZipArchive;

        if ($zip->open("{$dir}{$name}", ZipArchive::CREATE) === TRUE) {
            $zip->addFile(storage_path("app/documentos/{$this->persona->documento}"), $this->persona->documento);
            $zip->addFile(storage_path("app/fotos/{$this->persona->foto}"), $this->persona->foto);
            $zip->close();
        }

        return response()->download("{$dir}{$name}")->deleteFileAfterSend(true);
    }

    /**
     * Approve the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        $status = "Este registro fue aprobado con anterioridad.";
        if (!$this->persona->hasVerifiedId()) {
            $this->persona->markIdAsVerified();
            $this->persona->sendIdVerificationNotification();
            $status = "Registro aprobado con éxito, un correo ha sido enviado a \"{$this->persona->correo}\" para informar su cambio de estatus.";
        }

        return back()->withStatus($status);
    }

    public function payments($id)
    {
        $persona = $this->persona;
        return view('admin.pago.index', compact('persona'));
    }

    /**
     * Paid the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function paid($id)
    {
        if (!$this->pago->hasVerifiedPayment()) {
            $this->pago->markPaymentAsVerified();
            $this->pago->markLicensesAsPaid();
        }

        return back()->withStatus("El pago fue aprobado con éxito!");
    }

    public function licenses($id)
    {
        $persona = $this->persona;
        return view('admin.licencia.index', compact('persona'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showLicense($id)
    {
        $persona = $this->persona;
        $licencia = $this->licencia;

        return view('admin.licencia.show', compact('persona', 'licencia'));
    }

    /**
     * Approve the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approveLicense($id)
    {
        $status = null;
        $this->licencia->sendLicenseApprovalNotice();
        if (!$this->licencia->hasApprovedLicense()) {
            $this->licencia->markLicenseAsApproved();
            $status = "Licencia aprobada con éxito, un correo ha sido enviado a \"{$this->persona->correo}\" para informar su cambio de estatus.";
        }

        return back()->withStatus($status);
    }
    /**
     * Approve the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function aprobarsolicitud($id) 
    {
        $id = decrypt($id); 
        $serial = unique_random('solicitud_licencias', 'serial', 30);                
        DB::table('solicitud_licencias')
            ->where('id', $id)
            ->update([
                'estatus' => 'A',
                'f_estatus' => date("Y/m/d h:m:i"),
                'serial'=> $serial,
                'f_expiracion'=>date('Y/m/d', strtotime('+1 year')),
            ]);         
        $status = null;
        $this->solicitud_licencia->sendLicenseApprovalNotice();
        if (!$this->solicitud_licencia->hasApprovedLicense()) {
            $this->solicitud_licencia->markLicenseAsApproved();
            $status = "Licencia aprobada con éxito, un correo ha sido enviado a \"{$this->persona->correo}\" para informar su cambio de estatus.";
        }

        return Redirect::to('admin/solicitud_licencias')->withStatus($status);
    }    
}
