<?php

namespace App\Http\Controllers\Admin;

use Silber\Bouncer\Database\Ability;
use Silber\Bouncer\Database\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreRolesRequest;
use App\Http\Requests\Admin\UpdateRolesRequest;
use Bouncer;

class RolesController extends Controller
{
    protected $role;

    public function __construct(Request $request)
    {
        if (! is_null($id = $request->role)) {
            $this->role = Role::findOrFail(decrypt($id));
        }
    }

    /**
     * Display a listing of Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // if (! Gate::allows('perfiles')) {
     //       return abort(401);
     //   }

        $roles = Role::all();

        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating new Role.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // if (! Gate::allows('crear_perfil')) {
       //     return abort(401);
      //  }
        $abilities = Ability::pluck('name', 'name');

        return view('admin.roles.create', compact('abilities'));
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param  \App\Http\Requests\StoreRolesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRolesRequest $request)
    {
      //  if (! Gate::allows('crear_perfil')) {
      //      return abort(401);
     //   }

        $role = Role::create([
            'name' => trim(strtolower($request->input('name'))),
        ]);
        $role->allow($request->input('abilities'));

        return redirect()->route('admin_roles_index')->withStatus("Perfil registrado con éxito.");
    }


    /**
     * Show the form for editing Role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //  if (! Gate::allows('editar_perfil')) {
     //       return abort(401);
    //    }
        $role = Role::find(decrypt($id));
        $abilities = Ability::pluck('name', 'name');
       // $role = $this->role;

        return view('admin.roles.edit', compact('role', 'abilities'));
    }

    /**
     * Update Role in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //if (! Gate::allows('editar_perfil')) {
       //     return abort(401);
      //  }
       $rol = Role::find(decrypt($id));
        
      // $rol->fill([
        //   'name' => trim(strtolower($request->input('name')))
       //])->save();

     //$rol->disallow($request->input('abilities'));
     //var_dump($request->input('abilities'));die();
     
    //evaluar si hay diferencia entre lo que viene de la bd y lo que esta en el campo abilidades
     //para saber que abilidades colocarle al rol
    
    
    
     // var_dump($request->input('abilities'));die();
   //   $rol->allow($request->input('abilities'));
     // Bouncer::disallow($rol->all());
     // Bouncer::allow($rol)->to($request->input('abilities'));

      $rol->disallow($request->input('abilities'));
      $rol->allow($request->input('abilities'));

      return redirect()->route('admin_roles_index')->withStatus("Perfil actualizado con éxito.");
    }
 

    /**
     * Remove Role from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('eliminar_perfil')) {
            return abort(401);
        }

        $this->role->delete();

        return redirect()->route('admin.perfiles.index')->withStatus("Perfil eliminado con éxito.");
    }

    /**
     * Delete all selected Role at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('eliminar_perfil')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Role::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
