<?php

namespace App\Http\Controllers\Admin;

use DB;
use ZipArchive;
use App\Models\Persona;
use Illuminate\Http\Request;
use App\Models\Empresa;
use App\Http\Controllers\Controller;
use Redirect;


class SolicitudLicenciasController extends Controller
{
    protected $persona;

    protected $solicitud_licencia;

    protected $pago;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        $mineros = DB::table('solicitud_licencias')
            ->join('empresas', 'empresas.id', '=', 'solicitud_licencias.id_empresa') 
            ->select('solicitud_licencias.id','empresas.rif','empresas.razon_social','solicitud_licencias.f_solicitud','solicitud_licencias.f_estatus','solicitud_licencias.estatus')
            ->get();
        return view('admin.solicitud_licencias.index', compact('mineros'));
    }
    /**
     * Approve the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function aprobarsolicitud($id) 
    {
        $id = decrypt($id); 
        $serial = unique_random('solicitud_licencias', 'serial', 30);                
        DB::table('solicitud_licencias')
            ->where('id', $id)
            ->update([
                'estatus' => 'A',
                'f_estatus' => date("Y/m/d h:m:i"),
                'serial'=> $serial,
                'f_expiracion'=>date('Y/m/d', strtotime('+1 year')),
            ]);         
        $status = null;
        /*$this->solicitud_licencia->sendLicenseApprovalNoticeCI();
        if (!$this->solicitud_licencia->hasApprovedLicense()) {
            $this->solicitud_licencia->markLicenseAsApproved();
            $status = "Licencia aprobada con éxito, un correo ha sido enviado a \"{$this->persona->correo}\" para informar su cambio de estatus.";
        }*/
        return Redirect::to('admin/solicitud_licencias')->withStatus($status);
    }    
}
