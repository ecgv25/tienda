<?php

namespace App\Http\Controllers\Admin;

use DB;
use Mail;
use App\User;
use App\Tienda;
use Illuminate\Http\Request;
use Silber\Bouncer\Database\Role;
use Silber\Bouncer\Database\Ability;
use Silber\Bouncer\Database\Permissions;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Mail\UserCreationNotificationEmail;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use Bouncer;
class UsuarioController extends Controller
{
    protected $usuario;

    protected $password_tmp;

    public function __construct()
    {
        if (! is_null($id = request()->usuario)) {
            $this->usuario = Usuario::find(decrypt($id));
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // if (! Gate::allows('usuarios')) {
     //      return abort(401);
      //  }
     
        $usuarios = User::with('roles')->get();

        return view('admin.usuario.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // if (! Gate::allows('crear_usuario')) {
       //     return abort(401);
      //  }

        $perfiles = Role::pluck('name', 'name');
        $tiendas=Tienda::orderBy('id','DESC')->paginate(10);
        $data = array('perfiles' => $perfiles,
                      'tiendas' => $tiendas);  

        return view('admin.usuario.create')->with($data);

       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // if (! Gate::allows('crear_usuario')) {
        //    return abort(401);
        //}

       // $usuario = DB::transaction(function() use ($request) {

           $password = $this->password_tmp = str_random();
           
            //return User::create(
                //$request->merge(array_map('strtolower',$request->validated()))->validated() + ['clave' => bcrypt($this->password_tmp)]
            //);
            $usuario = DB::table('users')->insert([
                'name' => $request->get('name'),
                'email' => $request->get('correo'),
                'password' => bcrypt($password),
                'status' => 'A',//activo definir en la bd los estatus
                'cedula' => $request->get('cedula'),
                'nombre' => $request->get('nombre'),
                'apellido' => $request->get('apellidos'),
                'telefono_movil' => $request->get('telefono_movil'),
                'telefono_fijo' => $request->get('telefono_fijo'),
                'cargo' => $request->get('cargo'),
                'idTienda' => $request->get('tienda'),
                'unidad_administrativa' => $request->get('unidad_administrativa')
             ]);


      //  });
        $usuario  = User::orderby('created_at','DESC')->take(1)->first();
        $usuarioId = $usuario->id;
        Bouncer::assign($request->input('perfiles'))->to($usuarioId);
         Mail::to($usuario->email)->send(new UserCreationNotificationEmail($usuario, $password));
        return redirect()->route('admin.usuario.index')->withStatus("Usuario registrado con éxito, se le ha enviado un correo con su clave de acceso temporal.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //  if (! Gate::allows('editar_usuario')) {
      //      return abort(401);
      //  }

       // $usuario = $this->usuario;
        
        $usuario = User::find(decrypt($id));
        
        //eliminar un rol
        //Bouncer::retract('administrador')->from($usuario);
        //eliminar una abilidad
        //Bouncer::disallow( $usuario)->to('agregar-producto');
        //$usuario->disallow('agregar-producto');
       // Agregar-Producto Modificar-Producto Eliminar-Producto
        
        $perfiles = Role::pluck('name', 'name');
        $tiendas=Tienda::orderBy('id','DESC')->paginate(10);
        $abilidadesUsuario= $usuario->getAbilities();


      //Bouncer::allow('administrador')->to('reporte-producto');


    //  $permisos= Abilities::where('entity_id', '=', $perfilesId);
    //  $abilidades = Ability::all();
   //   Bouncer::retract('administrador')->from($usuario);
        $abilidadesUsuario= $usuario->getAbilities();
    
    //asigna un roll
       // Bouncer::assign('consulta')->to( $usuario);
      
      // hacer con la entidad permissions buscando por entity_id que es el id del role y luego en la tabla abilidades para saber cual es la hab
        return view('admin.usuario.edit', compact('usuario', 'perfiles','abilidadesUsuario', 'tiendas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateUsuario(Request $request, $id)
    {
        //if (! Gate::allows('editar_usuario')) {
        //    return abort(401);
       // }
        // Guardar datos de usuario
       // $this->usuario->fill(
        //    $request->merge(array_map('strtolower',$request->only(['cargo','unidad_administrativa'])))->except(['password', 'password_confirmation'])
       // )->save();
     //$usuario = User::find(decrypt($id));
     
     User::where("id", "=", decrypt($id))
     ->update(array(
        
         
            "cargo" => $request->get('cargo'),
            "unidad_administrativa" => $request->get('unidad_administrativa'),
            "telefono_movil" => $request->get('telefono_movil'),
            "telefono_fijo"=> $request->get('telefono_fijo'),
            "idTienda"=> $request->get('tienda'),
            
        ));
    $usuario = User::find(decrypt($id));  
    $roles = Role::pluck('name', 'name');
    foreach($roles as $rol):
        Bouncer::retract($rol)->from($usuario);
    endforeach;    
   
    Bouncer::assign($request->input('perfiles'))->to($usuario);

    //  if (! is_null($request->telefono_movil)) {
          //  $this->usuario->fill(['telefono_movil' => $request->telefono_movil])->save();
      //  }
        // Eliminar perfil(es) anterior(es)
       // 
        // Asignar perfil(es) nuevo(s)
     //   $this->usuario->assign($request->input('perfiles'));
        // Actualizar clave si se envia una nueva
       // if (! is_null($request->password)) {
        //    $this->usuario->fill(['clave' => bcrypt($request->password)])->save();
       // }

        return redirect()->route('admin.usuario.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
