<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Persona;
use DB;
use Redirect;

class VerificacionIdentidadController extends Controller
{
	protected $persona;
    //
    public function __construct()
    {
     
    }

    public function index(Request $request)
	{		
	    return view('admin.verificacion_identidad.index', []);
	}

	public function grid(Request $request)
	{	
		$url = config('app.url');		
		//actualizar estatus de personas para verificar identidad
        Persona::where('persona_verified_at',null)->where('motivo_rechazo',null)->where('documento','<>',null)->where('foto','<>',null)->update([
            'proceso_verificacion' => 0,
        ]);
        $correo = $_GET['search']['value'];              
        //si buscan un correo
		if($correo) 
		{				
			//buscar por un correo      
	        $persona_verificar = Persona::where('persona_verified_at',null)->where('motivo_rechazo',null)->where('documento','<>',null)->where('foto','<>',null)->where('proceso_verificacion',0)->where('correo',$correo)->first();        	
		}
		else
		{
			//buscar aleatoriamente una persona para verificar su identidad         
	        $persona_verificar = Persona::where('persona_verified_at',null)->where('motivo_rechazo',null)->where('documento','<>',null)->where('foto','<>',null)->where('proceso_verificacion',0)->inRandomOrder()->first();        	
		}
        //se modifica el valor de proceso_verificacion para que lo reserve
        if ($persona_verificar!=null)
        {
            Persona::where('id',$persona_verificar->id)->update([
            'proceso_verificacion' => 1,
            ]);
        }

        //armar el tr de la tabla
        if ($persona_verificar==null)
        {
        	$r[] = "Sin datos para mostrar";
        	$r[] = "";
        	$r[] = "";
        	$r[] = "";
        	$r[] = "";
        	$ret[] = $r;
			$ret['data'] = $ret;
			$ret['recordsTotal'] = 1;
			$ret['iTotalDisplayRecords'] = 1;

			$ret['recordsFiltered'] = count($ret);
			$ret['draw'] = $_GET['draw'];
	        
			echo json_encode($ret);
        }
        else
        {

	        $id_persona = encrypt($persona_verificar->id);
	        $r = [];
	        $r[] = $persona_verificar->correo." ".$persona_verificar->nombres." ".$persona_verificar->apellidos;
	        $documento = $url."/images/persons/documentos/".$persona_verificar->documento;
	        $selfie = $url."/images/persons/documentos/".$persona_verificar->foto;
	        $download = $url."/admin/persona/".encrypt($persona_verificar->id)."/download";
	        $r[] = '<a id="btnViewDocs" class="waves-effect" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#viewDocuments" data-type-doc="'.$persona_verificar->tipo_documento.'" data-doc="'.$documento.'" data-selfie="'.$selfie.'" data-download="'.$download.'" onclick="viewDocs()">
	        	<i class="fas fa-folder"></i>
	        	</a>';
	       	$r[] = '<a href="#" onclick="aprobar('."'".$id_persona."'".')" class="btn btn-outline-info btn-sm">
	       			<i class="fas fa-thumbs-up"></i>
	    				Aprobar identidad
	    			</a>';
	        $r[] = '<a href="" class="btn btn-outline-info btn-sm" id="abrirModalRechazar" data-toggle="modal" data-id="'.$id_persona.'" data-target=".modalRechazar" onclick="viewRechazar()">
					<i class="fas fa-thumbs-down"></i>
					Rechazar identidad
				</a>';
			$ret[] = $r;
	        //
	        $ret['data'] = $ret;
			$ret['recordsTotal'] = 1;
			$ret['iTotalDisplayRecords'] = 1;

			$ret['recordsFiltered'] = count($ret);
			$ret['draw'] = $_GET['draw'];
	        
			echo json_encode($ret);
		}       

	}	

	/**
     * Aprobacion the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function aprobacion($id)
    {       
        
        $persona = Persona::where('id',decrypt($id))->first(); 
        $status = "Este registro fue aprobado con anterioridad.";
        if (!$persona->hasVerifiedId()) {
            $persona->markIdAsVerified();
            $persona->sendIdVerificationNotification();
            $status = "Registro aprobado con éxito, un correo ha sido enviado a \"{$persona->correo}\" para informar su cambio de estatus.";
        }
        return "OK";        
    }
	/**
     * approve1 the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request     
     */
    public function no_aprobacion(Request $request)
    {                 	
        $id = $request->id_persona;
        $motivo = $request->motivo;        
        
        $persona = Persona::where('id',decrypt($id))->first();        
        $status = "Este registro fue rechazado con anterioridad.";
        if (!$persona->notHasVerifiedId()) {
            $persona->notMarkIdAsVerified($motivo);
            $persona->sendIdNotVerificationNotification($motivo);
            $status = "Registro rechazado con éxito, un correo ha sido enviado a \"{$persona->correo}\" para informar su cambio de estatus.";
        }
        return back()->withStatus($status);   
    }
	
}