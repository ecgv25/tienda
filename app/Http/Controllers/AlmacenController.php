<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Productos;
use App\Inventario;
use App\Tienda;
use App\Almacenes;
use App\User;
use Bouncer;
class AlmacenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //comentario para los permisos del bouncer
        // permision en el campo entity_id se coloca el id del rol o el id del usuario
        //
        //$//user = auth()->user();
        //Bouncer::allow('administador')->to('create-posts');
        //Bouncer::assign('administador')->to( $user);
        
       // Bouncer::allow('consulta')->to('reporte-producto');
       // Bouncer::allow('consulta')->to('reporte-inventario');
       // Bouncer::allow('consulta')->to('reporte-ventas');
      //  Bouncer::allow('consulta')->to('reporte-obsequio');
       // Bouncer::allow($user)->to('delete-posts');
       // $user->allow('post-comments');
        
       //$user->disallow('post-comments');
       // var_dump($user);
        $idTienda = auth()->user()->idTienda;           

        $almacenes=Almacenes::where('idTienda', '=', $idTienda )->orderBy('id','DESC')->paginate(10);
       
        $usuario = User::find(auth()->id());
       
        if($usuario->isAn('administrador')):
            $almacenes=Almacenes::orderBy('id','DESC')->paginate(10);
        endif;  
        
        return view('Almacen.index',compact('almacenes')); 
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Almacen.create');
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required']);
        Almacenes::create($request->all());
        return redirect()->route('almacen_index')->with('success','Registro creado satisfactoriamente');
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tienda=Tienda::find($id);
        return  view('Tienda.show',compact('Productos'));
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       
        $almacen=Almacenes::find($id);
      
        return view('Almacen.edit',compact('almacen'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)    {
        
       // $this->validate($request,[ 'nombre'=>'required', 'codigo'=>'required', 'descripcion'=>'required']);
 
        Almacenes::find($id)->update($request->all());
        return redirect()->route('almacen_index')->with('success','Registro actualizado satisfactoriamente');
 
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Almacenes::find($id)->delete();
        return redirect()->route('almacen_index')->with('success','Registro eliminado satisfactoriamente');
     }

      /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function agregarAlmacenTienda($id)
    {
        //
       
        $almacen=Almacenes::find($id);
        $tiendas=Tienda::orderBy('id','DESC')->paginate(10);
        $data = array('almacen' => $almacen,
                      'tiendas' => $tiendas);  

        return view('Almacen.agregar')->with($data);
    }
     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateAgregarAlmace(Request $request, $id)    {
        //
        //$this->validate($request,[ 'nombre'=>'required', 'codigo'=>'required', 'descripcion'=>'required']);
 
       
        $almacen=Almacenes::find($id);
        $asignacionAlmacen = Almacenes::where("id", "=",  $id)
                                                       ->update(array("idTienda" => $request->get('tienda'),
                                                               ));
        
        return redirect()->route('almacen_index')->with('success','Registro actualizado satisfactoriamente');
 
    }

    
    
}