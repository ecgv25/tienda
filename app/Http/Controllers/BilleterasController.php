<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Productos;
use App\Inventario;
use App\Tienda;
use App\User;
use App\Billeteras;
use Illuminate\Support\Facades\DB;
class BilleterasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $idTienda = auth()->user()->idTienda;
        $billeteras=Billeteras::where('idTienda', '=', $idTienda )->name($request->get('nombre'))->orderBy('id','DESC')->paginate(10);
        $usuario = User::find(auth()->id());
        if($usuario->isAn('administrador')):
            $billeteras=Billeteras::name($request->get('nombre'))->orderBy('id','DESC')->paginate(10);
        endif; 
        
        return view('Billeteras.index',compact('billeteras')); 
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Billeteras.create');
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idTienda = auth()->user()->idTienda;
        $this->validate($request,[ 'nombre'=>'required', 'hash'=>'required', 'codigo'=>'required','descripcion'=>'required' ,'direccion'=>'required','moneda'=>'required','telefono'=>'required']);
       // Billeteras::create($request->all());
        DB::table('billeteras')->insert([
            'nombre' => $request->get('nombre'),
            'codigo' => $request->get('codigo'),
            'hash' => $request->get('hash'),
            'descripcion' => $request->get('descripcion'),
            'direccion' =>  $request->get('direccion'),
            'telefono' =>  $request->get('telefono'),
            'moneda' => $request->get('moneda'),
            'idTienda' =>$idTienda
            

         ]); 
        return redirect()->route('billeteras_index')->with('success','Registro creado satisfactoriamente');
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tienda=Tienda::find($id);
        return  view('Tienda.show',compact('Productos'));
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       
        $billetera=Billeteras::find($id);

        return view('Billeteras.edit',compact('billetera'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)    {
        //
        $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required' , 'hash'=>'required','direccion'=>'required']);

 
        Billeteras::find($id)->update($request->all());
        return redirect()->route('billeteras_index')->with('success','Registro actualizado satisfactoriamente');
 
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $billetera=Billeteras::find($id);
        // las tiendas pueden ser eliminadas siempre que no tengas registros de productos o ventas.
       // para lo cual se debe buscar en las tablas inventario  y ventas por el id de la tienda.
       // if ( $producto or $inv) {
         // Si el producto tiene inventario y/o ventas no se puede eliminar
      //   return redirect()->route('tienda_index', ['id' => $id])->with('success','la tienda no puede ser eliminada ya que presenta inventario o ventas activas');

      //  }else{

            Billeteras::find($id)->delete();
            return redirect()->route('billeteras_index')->with('success','Registro eliminado satisfactoriamente');
        
      //  }

       
    
    
    }
}