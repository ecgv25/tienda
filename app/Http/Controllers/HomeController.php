<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventario;
use App\ProductosVentas;
use App\Ventas;
use App\Tienda;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $idTienda = auth()->user()->idTienda;
       
        //tienda asociada al usuario
        $tienda=Tienda::find( $idTienda);
        //productos vendidos por la tienda del usuario
        $productos = ProductosVentas::where('idTienda', '=', $idTienda )->count();
        //cantidad total de ventas de la tienda del usuario
        $ventas = Ventas::where('idTienda', '=', $idTienda )->get();
        //cantidad de petros de las ventas del usuario
       
        $petros=0;
        foreach ($ventas as $venta) {
           $petros+=$venta->montoTotal;
        }
       
        $cantidadVentas=$ventas->count();
        $data = array(
        'ventas' => $cantidadVentas,
        'petros' => $petros,
        'tienda' => $tienda,
        'productos' => $productos);  

        return view('home')->with($data);
        
        
    }
}
