<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Inventario;
use App\Productos;
use App\Tienda;
use App\Ventas;
use App\ProductosVentas;
use App\Almacenes;
use Illuminate\Support\Facades\DB;
use App\Exports\InventarioExport;
use App\Imports\VentasImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
 
class InventarioController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $idTienda = auth()->user()->idTienda;
        $inventario=Inventario::where('idTienda', '=', $idTienda )->name($request->get('nombre'))->orderBy('id','DESC')->paginate(10);
        return view('Inventario.index',compact('inventario')); 
    }
    
    /**
     * @return \Illuminate\Http\Response
     */
    public function new()
    {
        $idTienda = auth()->user()->idTienda;
        $tiendas=Tienda::find($idTienda);
        $productos = DB::table('productos')->get();
        $almacenes='';
        $almacenes=Almacenes::where('idTienda', '=', $idTienda )->get();
        $data = array('productos' => $productos,
        'almacenes' => $almacenes,
        'tiendas' => $tiendas);  

        return view('Inventario.new')->with($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {      

        //
        return view('Inventario.create');
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[ 'idProducto'=>'required', 'cantidad'=>'required' ,'idTienda'=>'required', 'idAlmacen'=>'required']);
        $producto = Productos::where('id', '=', $request['id'])->first();

        $observacion = 'se agrego producto al inventario'.'cantidad:'.$request['cantidad'].'producto:' ;

        DB::table('movimientos_inventario')->insert([
            'idProducto' =>  $request['idProducto'],
            'observacion' =>  $observacion,
            'idTipoMovimiento' => '1',
            ]);

            $idProducto=$request['idProducto'];
            $idAlmacen= $request['idAlmacen'];
            $idTienda=$request['idTienda'] ;
                                    
            $producto = Inventario::where(['idProducto' => $idProducto,'idAlmacen' => $idAlmacen,'idTienda' =>   $idTienda])->first();
            if($producto):
                        //hacer el update con los nuevos campos
                $mod_cantidad_inventario = Inventario::where(['idProducto' => $idProducto,'idAlmacen' => $idAlmacen,'idTienda' =>   $idTienda])
                                                                ->update(array(
                                                                "cantidad" => $request['cantidad']+$producto->cantidad,
                                                                ));   
                    else:
                        Inventario::create($request->all());
                endif;    


        return redirect()->route('inventario_index')->with('success','Registro creado satisfactoriamente');
    }
 
      /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajuste($id)
    {
     
        $inv=Inventario::find($id);
        $producto = Productos::where('id', '=', $inv->idProducto)->first();
        $data = array('producto' => $producto,'inv' =>$inv);  

        return view('Inventario.ajuste')->with($data);
    }    
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function retirarProductoInventario(Request $request)
    {
     DB::table('movimientos_inventario')->insert([
            'idProducto' =>  $request['idProducto'],
            'observacion' =>  $request['observacion'],
            'idTipoMovimiento' => '2',
            ]);
         //bucar la cantidad del producto
            $producto = Inventario::where('idProducto', '=', $request['idProducto'])->first();
            if($producto):
                $cantidad = $producto->cantidad - $request['cantidad'];
                    //hacer el update con los nuevos campos
                    $mod_cantidad_inventario = Inventario::where("idProducto", "=", $request['idProducto'])
                    ->update(array(
                        "cantidad" => $cantidad,
                    ));   
            endif;    


        return redirect()->route('inventario_index')->with('success','Registro creado satisfactoriamente');
    }  


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto=Productos::find($id);
        return  view('Productos.show',compact('Productos'));
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       
        $inv=Inventario::find($id);

        return view('Inventario.edit',compact('inv'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)    {
        //
       // $this->validate($request,[ 'nombre'=>'required', 'codigo'=>'required', 'descripcion'=>'required']);
 
        Inventario::find($id)->update($request->all());
        return redirect()->route('inventario_index')->with('success','Registro actualizado satisfactoriamente');
 
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Inventario::find($id)->delete();
        return redirect()->route('inventario_index')->with('success','Registro eliminado satisfactoriamente');
 
    }
    public function export() 
    {
        return Excel::download(new InventarioExport, 'inventario.xlsx');
    }
    
    public function import() 
    {
        return Excel::import(new InventarioExport, 'users.xlsx');
    }
    
     /**
     * Ajax para verificar el costo del producto en el inventario
     */
    public function getAlmacenes(Request $request) {
        $idTienda = $request['idTienda'];
        $almacenes = Almacenes::where('idTienda', '=', $idTienda)->get();
        $data['almacenes'] = $almacenes;
        return response()->json($data);  
    }

/**
     * @return \Illuminate\Http\Response
     */
    public function cierre()
    {
        $idTienda = auth()->user()->idTienda;
        $tiendas=Tienda::find($idTienda);
        $productos = DB::table('productos')->get();
        $almacenes='';
        $fecha_cierre = date('Y-m-d');
        $fecha_cierre_format = date('d-m-Y');
        $ventas=Ventas::where([
                                'fecha' => $fecha_cierre ,
                                'idTienda' =>   $idTienda
                                ])->get();
        
        $petros=0;
        $bitcoin=0;
        $dash=0;
        $ether=0;
        $litecoin=0;
        $ganancia=0;
        $total=0;
        $totalVentas=0;
        $gananciaPromedio=0;

        foreach ($ventas as $venta) {

            $totalVentas+=$venta->montoTotal;
           
           if($venta->moneda == 'petro'):
            $ventasPtr=Ventas::where([
                'fecha' => $fecha_cierre ,
                'idTienda' =>   $idTienda,
                'moneda' => 'petro'
                ])->get();

                foreach ($ventasPtr as $vent) {
                     $petros+=$vent->montoTotal;
                }
            endif;

            if($venta->moneda == 'bitcoin'): 
                    
                    $ventasBtc=Ventas::where([
                        'fecha' => $fecha_cierre ,
                        'idTienda' =>   $idTienda,
                        'moneda' => 'bitcoin'
                        ])->get();

                foreach ($ventasBtc as $vent) {    
                    $bitcoin+=$vent->montoTotal;
                }


            endif;
            if($venta->moneda == 'dash'): 
                    $ventasDash=Ventas::where([
                        'fecha' => $fecha_cierre ,
                        'idTienda' =>   $idTienda,
                        'moneda' => 'dash'
                        ])->get();
                foreach ($ventasDash as $vent) {           
                    $dash+=$vent->montoTotal;
                }
            endif;
            if($venta->moneda == 'ether'): 
                    $ventasEth=Ventas::where([
                        'fecha' => $fecha_cierre ,
                        'idTienda' =>   $idTienda,
                        'moneda' => 'ether'
                        ])->get();
                foreach ($ventasEth as $vent) {            
                    $ether+=$vent->montoTotal; 
                }    
            endif;
            if($venta->moneda == 'litecoin'): 
                    $ventasLtc=Ventas::where([
                        'fecha' => $fecha_cierre ,
                        'idTienda' =>   $idTienda,
                        'moneda' => 'litecoin'
                        ])->get();
                foreach ($ventasLtc as $vent) {             
                    $litecoin+=$vent->montoTotal;      
                }         
           endif; 
           

           $productosVenta  = ProductosVentas::where('idVenta', '=', $venta ->id)->get();
           foreach ($productosVenta as $producto) {
               $ganancia+=$producto->gananciaProducto;
               $gananciaPromedio = $ganancia/$productosVenta->count();
               
            }
        }
       
        $total=$petros+$bitcoin+$dash+$ether+$litecoin;

        $data = array(
        'ventas' => $ventas,
        'productos' => $productos,
        'petros' => $petros,
        'bitcoin' => $bitcoin,
        'dash' => $dash,
        'ether' => $ether,
        'litecoin' => $litecoin,
        'total' => $total,
        'totalVentas' => $totalVentas, 
        'fecha_cierre_format' =>  $fecha_cierre_format,
        'fecha_cierre' => $fecha_cierre,
        'gananciaPromedio' => $gananciaPromedio,
        'tiendas' => $tiendas);  

        return view('Inventario.cierre')->with($data);
    }

}