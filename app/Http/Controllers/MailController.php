<?php
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use App\Mail\ReciboEmail;
use App\Mail\NotificacionEmail;
use Illuminate\Support\Facades\Mail;
use App\Ventas;
use App\ProductosVentas;



 
class MailController extends Controller
{
    public function send($id)
    {
        $objRecibo = new \stdClass();

        $venta = Ventas::find($id);
        $productosVenta  = ProductosVentas::where('idVenta', '=', $id)->get();
        

        $objRecibo->venta = $venta;
        $objRecibo->productos = $productosVenta;

        $mail = new ReciboEmail($objRecibo);
       
        
        Mail::to($venta->correo)->send($mail);
        return redirect()->route('ventas_index')->with('success','Registro creado satisfactoriamente');


    }
    public function EnviarNotificacion($usuario)
    {
        $objNotificacionUsuario = new \stdClass();


        
        
        $objNotificacionUsuario->venta = $venta;
      
        
        $mail = new NotificacionEmail($objNotificacionUsuario);
        
        Mail::to($venta->correo)->send($mail);
        return redirect()->route('ventas_index')->with('success','Registro creado satisfactoriamente');


    }
}