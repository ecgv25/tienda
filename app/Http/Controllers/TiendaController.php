<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Productos;
use App\Inventario;
use App\Tienda;
use App\User;

 
class TiendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $idTienda = auth()->user()->idTienda;
        $tiendas=Tienda::where('id', '=', $idTienda )->name($request->get('name'))->orderBy('id','DESC')->paginate(10);
        $usuario = User::find(auth()->id());

         //puede ver todas las tiendas si es un usuario administrador
        if($usuario->isAn('administrador')):
             $tiendas=Tienda::name($request->get('name'))->orderBy('id','DESC')->paginate(10);
        endif;    

        return view('Tienda.index',compact('tiendas')); 
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Tienda.create');
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required' , 'rif'=>'required','direccion'=>'required','correo'=>'required']);
        Tienda::create($request->all());
        return redirect()->route('tienda_index')->with('success','Registro creado satisfactoriamente');
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tienda=Tienda::find($id);
        return  view('Tienda.show',compact('Productos'));
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       
        $tienda=Tienda::find($id);

        return view('Tienda.edit',compact('tienda'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)    {
        //
        $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required' , 'rif'=>'required','direccion'=>'required']);

 
        Tienda::find($id)->update($request->all());
        return redirect()->route('tienda_index')->with('success','Registro actualizado satisfactoriamente');
 
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $tienda=Tienda::find($id);
       // para lo cual se debe buscar en las tablas inventario  y ventas por el id de la tienda.
        $productos = Inventario::where('idTienda', '=', $tienda->id )->get();
    
        if ($productos) {
         // Si el producto tiene inventario y/o ventas no se puede eliminar
            return redirect()->route('tienda_index', ['id' => $id])->with('success','la tienda no puede ser eliminada ya que presenta inventario o ventas activas');

         }else{
        // las tiendas pueden ser eliminadas siempre que no tengas registros de productos o ventas.

            Tienda::find($id)->delete();
            return redirect()->route('tienda_index')->with('success','Registro eliminado satisfactoriamente');
        
        }

       
    
    
    }
}