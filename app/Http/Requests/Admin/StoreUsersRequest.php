<?php
namespace App\Http\Requests\Admin;

use App\Rules\Cedula;
use App\Rules\Telefono;
use App\Rules\AlphaSpace;
use Illuminate\Foundation\Http\FormRequest;

class StoreUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cedula' => [
                'required',
                new Cedula,
                'unique:usuarios'
            ],
            'nombres' => ['required',new AlphaSpace, 'max:100'],
            'apellidos' => ['required',new AlphaSpace, 'max:100'],
            'correo' => 'required|email|max:191|unique:usuarios',
            'genero' => 'required|in:f,m',
            'telefono_movil' => [
                'required',
                new Telefono,
                'unique:usuarios'
            ],
            'telefono_fijo' => [
                'nullable',
                new Telefono,
            ],
            'cargo' => 'required|string|max:191',
            'unidad_administrativa' => 'required|string|max:191',
        ];
    }
}
