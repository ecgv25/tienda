<?php
namespace App\Http\Requests\Admin;

use App\Rules\Telefono;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->usuario;

        return [
            'telefono_movil' => [
                'required',
              //  new Telefono,
                "unique:usuarios,telefono_movil,{$id}"
            ],
            'telefono_fijo' => [
                'nullable',
             //   new Telefono,
            ],
            'password' => 'nullable|confirmed|min:8',
            'cargo' => 'required|string|max:191',
            'unidad_administrativa' => 'required|string|max:191',
        ];
    }
}
