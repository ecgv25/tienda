<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    protected $fillable = ['id', 'idProducto', 'cantidad' ,'costoDivisas','costoPetros','ganancia','idTienda','idAlmacen',];
   
   
    /**
	* Get data Productos
	*/
	public function productos()
	{
        return $this->belongsTo('App\Productos','idProducto');
	}
	
	/**
	* Get data Tienda
	*/
	public function tienda()
	{
        return $this->belongsTo('App\Tienda','idTienda');
	}
	
	/**
	* Get data Almacen
	*/
	public function almacen()
	{
        return $this->belongsTo('App\Almacenes','idAlmacen');
	}
	public function scopeName($query, $name)
	{

	 $query->where('id', "LIKE","%$name%");

	// $query->where('productos.nombre', 'LIKE', '%' . $name . '%')
	    //   ->join('productos', 'inventarios.idProducto', '=', 'productos.id ');
	
	 return $query; 
	}
}