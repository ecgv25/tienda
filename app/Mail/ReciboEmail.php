<?php
 
namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class ReciboEmail extends Mailable
{
    use Queueable, SerializesModels;
     
    /**
     * 
     * @var Recibo
     */
    public $recibo;
    /**
     * 
     * @var Productos
     */
    public $productos;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($recibo)
    {
        $this->recibo = $recibo;
        //$this->productos = $productos;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      
        return $this->from('carolina.g.ve@gmail.com')
                    ->view('mails.recibo')
                    ->subject('Recibo de Compra');
                   // ->attach(public_path('/images').'/demo.jpg', [
                     //         'as' => 'demo.jpg',
                       //       'mime' => 'image/jpeg',
                     // ]);
    }
}