<?php
 
namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class UserCreationNotificationEmail extends Mailable
{
    use Queueable, SerializesModels;
     
    /**
     * 
     * @var Usuario
     * @var Password
     */
    public $usuario;
    public $password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($usuario,$password)
    {
        $this->usuario = $usuario;
        $this->password = $password;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      
        return $this->from('carolina.g.ve@gmail.com')
                    ->view('mails.notificacion_usuario')
                    ->subject('Creacion de Usuario');
                   // ->attach(public_path('/images').'/demo.jpg', [
                     //         'as' => 'demo.jpg',
                       //       'mime' => 'image/jpeg',
                     // ]);
    }
}