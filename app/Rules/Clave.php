<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Clave implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $passes = true;
        // Validar si tiene al menos una letra mayúscula.
        if (! preg_match('/[A-Z]+/', $value)) {
            $passes = false;
        }
        // Validar si tiene al menos una letra minúscula.
        if (! preg_match('/[a-z]+/', $value)) {
            $passes = false;
        }
        // Validar si tiene al menos un número o carácter especial.
        if (! preg_match('/[\d\#\$\.\-\_\,]+/', $value)) {
            $passes = false;
        }
        return $passes;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'La '. __('Password') . ' debe contener al menos una letra mayúscula, una minúscula y un número o un carácter especial (#$.-_,)';
    }
}
