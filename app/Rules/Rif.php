<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Rif implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('/^[VvEeJjGg]{1}\-{1}\d{8}\-{1}\d{1}$/', $value);         
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El número de :attribute no cumple con el formato.';
    }
}
