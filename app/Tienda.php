<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tienda extends Model
{
    protected $fillable = ['id', 'nombre', 'codigo' ,'descripcion','direccion','telefono','rif','correo'];
	
	/**
	* Get data Almacen
	*/
	public function almacenes()
	{
        return $this->belongsTo('App\Almacenes','idTienda');
	}

	public function scopeName($query, $name)
	{

	 $query->where('nombre', "LIKE","%$name%");

		
                   
	}
}
