<?php
use Bouncer;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(User::class)->create([
            'name' => 'Foo Bar',
            'email' => 'foo@styde.net',
            'password' => bcrypt('secret')
        ]);
 
        $creatorOfPosts = factory(User::class)->create([
            'name' => 'Creator of Posts',
            'email' => 'creator@styde.net',
            'password' => bcrypt('secret')
        ]);

        Bouncer::allow('admin')->everything();
 
        // Asignamos el rol de administrador a $user
        $user->assign('admin');
 
        // Asignamos al autor la habilidad de crear posts
        $creatorOfPosts->allow('create', Post::class);
    }
}
