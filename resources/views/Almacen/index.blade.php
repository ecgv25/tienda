@extends('layouts.app')

@section('htmlheader_title', 'Almacenes')

@section('content_title', 'Almacenes')

@section('content')

			@if(Session::has('success'))

      <div class="alert alert-info alert-dismissable col-10">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
				{{Session::get('success')}}
			</div>
			@endif
 
  <div class="card">
      <div class="card-body">
            <h4 class="card-title">
                Listado de Almacenes
            </h4>
            <div class="pull-right">  
            <div class="btn-group">
            
            <a href="{{ route('almacen_create') }}" class="btn btn-outline-info btn-lg">
                                Registrar Almacen
                            </a>
                </div>
          </div>
          </div>
           <div class="table-responsive">
              <table class="table table-hover">
             <thead>
               <th>N</th>
               <th>Nombre</th>
               <th>Descripcion</th>
               <th>Codigo</th>
               <th>Tienda Asignada</th>
               <th>Editar</th>
               @can ('delete-posts')
               <th>Eliminar</th>
               @endcan
  

             </thead>
             <tbody>
             <?php $i= 1; ?>
              @if($almacenes->count() > 0)  
              
              @foreach($almacenes as $almacen)  
              <tr>
              <td>{{$i}}</td>
                <td>{{$almacen->nombre}}</td>
                
                <td>{{$almacen->descripcion}}</td>
                <td>{{$almacen->codigo}}</td>
                @if($almacen->tienda)
                <td>{{$almacen->tienda->nombre }}</td>
                @else
                <td><a class="btn btn-outline-info btn-xs" href="{{action('AlmacenController@agregarAlmacenTienda', $almacen->id)}}" ><span class="glyphicon glyphicon-plus">Asignar a Tienda </span></a></td>

                @endif

                <td><a class="btn btn-primary btn-xs" href="{{action('AlmacenController@edit', $almacen->id)}}" ><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td>
                  
                  <form action="{{action('AlmacenController@destroy', $almacen->id)}}" method="post">
                   {{csrf_field()}}
                   @can ('delete-posts')
                   <input name="_method" type="hidden" value="DELETE">
                  
                   <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                 </td>
                 @endcan
               </tr>
               <?php $i= $i+1; ?>
               @endforeach 
               @else
               <tr>
              
                <td colspan="8"> <div class="alert alert-danger">No hay almacenes registrados !! </div></td>
               
              </tr>
              @endif
            </tbody>
 
          </table>
          {{ $almacenes->links() }}
        </div>
                
        </div>
          
        </div>
       
      <div class="col col-lg-1"></div>
	</div>
@endsection