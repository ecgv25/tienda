@extends('layouts.app')

@section('htmlheader_title', 'Billeteras')

@section('content_title', 'Billeteras')

@section('content')

			@if(Session::has('success'))

      <div class="alert alert-info alert-dismissable col-10">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
				{{Session::get('success')}}
			</div>
			@endif
 
  <div class="card">
      <div class="card-body">
            <h4 class="card-title">
                Listado de Billeteras
            </h4>
            <div class="btn-group">
            <div class="col col-lg-12">
            {!! Form::open(['route' => 'billeteras_index', 'method' => 'GET', 'class' => ''])!!}
                {!! Form::text('name',null,['class' => 'form-control']) !!}
            </div>
            <div class="col col-lg-1">
                <button class="btn btn-outline-info btn-lg" type="submit">Buscar</button>
                {!! Form::close()!!}
                </div>   
                </div>
            
            <div class="pull-right">  
            
            <div class="btn-group">
            <a href="{{ route('billeteras_new') }}" class="btn btn-outline-info btn-lg">Registrar billetera
            </a>
            </div>
          </div>
          </div>
           <div class="table-responsive">
              <table class="table table-hover">
             <thead>
               <th>N</th>
               <th>Codigo</th>
               <th>Nombre</th>
               <th>Descripcion</th>
               <th>Telefonos</th>
               <th>Moneda</th>
               <th>hash</th>
               <th>Editar</th>
               <th>Eliminar</th>
              
             </thead>
             <tbody>
             <?php $i= 1; ?>
              @if($billeteras->count() > 0)  
              
              @foreach($billeteras as $billetera)  
              <tr>
              <td>{{$i}}</td>
                <td>{{$billetera->codigo}}</td>
                <td>{{$billetera->nombre}}</td>
                <td>{{$billetera->descripcion}}</td>
                <td>{{$billetera->telefono}}</td>
                <td>{{$billetera->moneda}}</td>
                <td>{{$billetera->hash}}</td>

                <td><a class="btn btn-primary btn-xs" href="{{action('BilleterasController@edit', $billetera->id)}}" ><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td>
                  <form action="{{action('BilleterasController@destroy', $billetera->id)}}" method="post">
                   {{csrf_field()}}
                   <input name="_method" type="hidden" value="DELETE">
 
                   <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                 </td>
               </tr>
               <?php $i= $i+1; ?>
               @endforeach 
               @else
               <tr>
              
                <td colspan="8"> <div class="alert alert-danger">No hay billeteras registradas !! </div></td>
               
              </tr>
              @endif
            </tbody>
 
          </table>
          {{ $billeteras->links() }}
        </div>
                
        </div>
          
        </div>
       
      <div class="col col-lg-1"></div>
	</div>
@endsection