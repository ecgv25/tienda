@extends('layouts.app')
@section('content_title', 'Ajuste de Inventario')
@section('content')
@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif
 
			<div class="card">
			
				<div class="card-body">	
						<form method="POST" id="inventario-ajuste-form" class="floating-labels m-t-20" action="{{ route('inventario_retirar') }}"  role="form">
							{{ csrf_field() }}
							<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
									<label for="producto">Producto</label>
										<input type="text" name="producto" id="producto" class="form-control input-sm" value="{{$inv->productos->nombre}}">
										<span class="bar"></span>
									</div>	
								</div>
								
										<input type="hidden" name="idProducto" id="idProducto" class="form-control input-sm" value="{{$inv->productos->id}}">
										
								
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
									<label for="idAlmacen">Almacen</label>
										<input type="text" name="idAlmacen" id="idAlmacen" class="form-control input-sm" value="{{$inv->almacen->nombre}}">
										<span class="bar"></span>
									</div>	
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
									<label for="idTienda">Tienda</label>
										<input type="text" name="idTienda" id="idTienda" class="form-control input-sm" value="{{$inv->tienda->nombre}}">
										<span class="bar"></span>
									</div>	
								</div>

								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
									<label for="costoPetros">Cantidad</label>
										<input type="text" name="cantidad" id="cantidad" class="form-control input-sm" value="{{$inv->cantidad}}">
									</div>
								</div>
							
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
									<label for="producto">Observacion</label>

										<textarea class="form-control" rows="5" id="comment" name="observacion" id="observacion" class="form-control input-sm"></textarea>
										<span class="bar"></span>
									</div>
								</div>
				
							</div>
 
							<div class="form-group m-t-40">
								<div class="btn-group">
									<a href="{{ route('inventario_index') }}" class="btn btn-outline-secondary btn-rounded" >Atrás</a>
									
									</a>
									<input type="submit"  value="Registrar" class="btn btn-outline-info btn-rounded">
									</button>
                            	</div>
                        	</div>	


						</form>
					</div>
				</div>
 
	
		</div>
	</section>
	@endsection