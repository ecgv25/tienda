@extends('layouts.app')
@section('content_title', 'Cierre')
@section('content')
@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif
			<div class="card">
			
				<div class="card-body">		
						<form method="POST" class="floating-labels m-t-20" action="{{ route('inventario_store') }}"  role="form">
							{{ csrf_field() }}
						<div class="row">
							
							
							
								<!--div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
									<label for="saldo">Saldo en la Billetera (petros) al {{$fecha_cierre_format}}</label>
										<input type="text" name="saldo" id="saldo" class="form-control input-sm" required >
									   	<span class="bar"></span>
									</div>
								</div-->
							<div class="col-xs-4 col-sm-4 col-md-4">	
								<div class="card">
							
									<div class="card-body">	
									<h5 class="card-title">Totales Billeteras al {{$fecha_cierre_format}} </h5><BR>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
											<p>PTR = 	{{$petros}}</p>
											</div>
										
											<div class="form-group">
											<p>BTC = 	{{$bitcoin}}</p>
												
											</div>
										
											<div class="form-group">
											<p>ETH = 	{{$ether}}</p>
											</div>
											<div class="form-group">
											<p>LTC = 	{{$litecoin}}</p>
											</div>

											<div class="form-group">
											<p>DASH = 	{{$dash}}</p>
											</div>

											<div class="form-group">
											<p>TOTAL = 	{{$total}}</p>
											</div>
										</div>
									</div>
								</div>
							</div>		
					
						<div class="col-xs-6 col-sm-6 col-md-6">	
									<div class="card">
									
										<div class="card-body">	
										<table class='table'> 
										<tr>
											<td colspan="4">Listado de Ventas Realizadas {{$fecha_cierre_format}}</td>
										</tr>
											<tr>
											<td>N</td>
											<td>Cant Prod</td>
											<td>Fecha y hora</td>
											<td>Monto</td>
											
										</tr>
										<?php $i= 1; ?> 	
										@foreach($ventas as $venta)  
											<tr>							
												<td>{{$i}}</td>
												<td>{{$venta->cantidad}}</td>
												<td>{{$venta->created_at->format('d/m/Y h:s a')}}</td>
												<td>{{$venta->montoTotal}}</td>
												
											</tr>
											<?php $i= $i+1; ?>
											@endforeach 
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td>Total {{$totalVentas}}</td>
											</tr>
										</table>	
										
										</div>
									</div>	
								</div>
						</div>
 							<div class="form-group m-t-40">
								<div class="btn-group">
									<a href="{{ route('inventario_index') }}" class="btn btn-outline-secondary btn-rounded" >Atrás</a>
									
									</a>
									<input type="submit"  value="Cerrar" class="btn btn-outline-info btn-rounded">
									</button>
                            	</div>
                        	</div>		
						</form>
					</div>
				</div>
			   </div>
			</div>
		</div>
	</section>

	<script type="text/javascript">
	$(function(){
		$('#costoDivisas, #ganancia').keyup(function(){
			var costoDivisa = parseFloat($('#costoDivisas').val());
			var porcentajeGanancia = parseFloat($('#ganancia').val());
			var valorPetroDolar = 60;

			if(isNaN(parseFloat(costoDivisa)) == false && isNaN(parseFloat(porcentajeGanancia)) == false) {
				var ganancia = (costoDivisa*(porcentajeGanancia/100));
				var totalCosto = costoDivisa + ganancia;
				var costoPetro = totalCosto / valorPetroDolar;

				$('#costoPetros').val(costoPetro.toFixed(8));
			}
		});
	});
	</script>

	<script type = "text/javascript">
		$('#idTienda').change(function(){
				$.ajax({
    				type: "GET",
    				url: "{{ route('inventario_search') }}",
    				data: {'idTienda': $(this).val(),
           			},
    				success: function(data) {
							if(data.almacen == null) {
								$('#idAlamacen').html(data.almacenes);

							} else {
								
								$('#idAlamacen').html(data.almacenes);
						
						}
					}
  				});
			});
	</script>
	@endsection

			