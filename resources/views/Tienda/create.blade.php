@extends('layouts.app')
@section('content_title', 'Registro de Tienda')
@section('javascripts')
<script>
$(document).ready(function () {
    $('#tienda-form').validate({
		focusInvalid: true,
		errorElement: "p",
		errorClass: "invalid",
		errorPlacement: function(error, element) {
    		error.insertAfter(element);
  		}, 
        rules: {
            nombre: {
                required: true
            },
			rif: {
                required: true
            },
			codigo: {
				required: true,
				digits: true,
				minlength: 3
			},
			descripcion: {
				required: true
			},
			direccion: {
				required: true
			},
			correo: {
				required: true
			},
			telefono: {
				required: true
			}
        },
		messages: {
			nombre: "Debe indicar un nombre de la tienda.",
			rif: "Debe indicar el rif de la tienda.",
			codigo: {
				required: "Debe indicar un codigo para la tienda",
				digits: "Solo puede ingresar numeros.",
				minlength: "el codigo debe tener una cantidad mínima de 3 dígitos.",
			},
			descripcion: "Debe idicar una descripcion",
			direccion: "Debe idicar una descripcion de la tienda",
			correo: "Debe idicar un correo, tome en cuenta que debe ser el mismo indicado en el momento que se registró en el sistema petro compra",
			telefono: "Debe idicar un telefono de contacto",
		}
    });
});
</script>
@endsection
@section('content')

			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif
 
			<div class="card">
			
				<div class="card-body">					
						<form method="POST" id="tienda-form" class="floating-labels m-t-20" action="{{ route('tienda_store') }}"  role="form">
							{{ csrf_field() }}

 							<div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <input id="nombre" type="text" class="text-capitalize form-control" name="nombre" value="" required>
                                            <span class="bar"></span>
                                            <label for="nombres">Nombre</label>

                                            <div class="form-control-feedback">
                                            </div>
                                        </div>
									</div>
									<div class="col-6">
									<div class="form-group">
											<input id="rif" type="text" class="text-capitalize form-control" name="rif" value="" required>
											<span class="bar"></span>
											<label for="rif">Rif</label>	
											<div class="form-control-feedback">
											</div>
									</div>
								</div>

 							</div>

							<div class="row">
                			
								<div class="col-6">
									<div class="form-group">
											<input id="codigo" type="text" class="text-capitalize form-control" name="codigo" value="">
											<span class="bar"></span>
											<label for="codigo">Codigo</label>	
											<div class="form-control-feedback">
											</div>
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
									<input id="descripcion" type="text" class="text-capitalize form-control" name="descripcion" value="" required>

									<span class="bar"></span>
									<label for="descripcion">Descripción</label>	
									<div class="form-control-feedback">
											</div>

									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
									<input id="direccion" type="text" class="text-capitalize form-control" name="direccion" value="" required>

									<span class="bar"></span>
									<label for="direccion">Direccion</label>	
									<div class="form-control-feedback">
									</div>

									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
									<input id="correo" type="text" class="text-capitalize form-control" name="correo" value="" required>

									<span class="bar"></span>
									<label for="correo">Correo</label>	
									<div class="form-control-feedback">
									</div>

									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
									<input id="telefono" type="text" class="text-capitalize form-control" name="telefono" value="">

									<span class="bar"></span>
									<label for="telefono">Telefono</label>	
									<div class="form-control-feedback">
								 </div>

									</div>
								</div>
							</div>
 
						
							
					
						  	<div class="form-group m-t-40">
								<div class="btn-group">
								<a href="{{ route('productos_index') }}" class="btn btn-outline-secondary btn-rounded" >Atrás</a>
									
									</a>
									<input type="submit"  value="Registrar" class="btn btn-outline-info btn-rounded">
									</button>
                            	</div>
                        	</div>	
						</form>
					</div>
				
 
			</div>

	@endsection