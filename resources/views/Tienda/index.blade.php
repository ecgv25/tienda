@extends('layouts.app')

@section('htmlheader_title', 'Tiendas')

@section('content_title', 'Tiendas')

@section('content')

			@if(Session::has('success'))

      <div class="alert alert-info alert-dismissable col-10">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
				{{Session::get('success')}}
			</div>
			@endif
 
  <div class="card">
      <div class="card-body">
            <h4 class="card-title">
                Listado de Tiendas
            </h4>
            <div class="btn-group">
            <div class="col col-lg-12">
            {!! Form::open(['route' => 'tienda_index', 'method' => 'GET', 'class' => ''])!!}
                {!! Form::text('name',null,['class' => 'form-control']) !!}
            </div>
            <div class="col col-lg-1">
                <button class="btn btn-outline-info btn-lg" type="submit">Buscar</button>
                {!! Form::close()!!}
                </div>   
                </div>
            
            <div class="pull-right">  
            
            <div class="btn-group">
            <a href="{{ route('tienda_create') }}" class="btn btn-outline-info btn-lg">Registrar Tienda
            </a>
            </div>
          </div>
          </div>
           <div class="table-responsive">
              <table class="table table-hover">
             <thead>
               <th>N</th>
               <th>Nombre</th>
               <th>Rif</th>
               <th>Descripcion</th>
               <th>Codigo</th>
               <th>Telefonos</th>
               <th>Direccion</th>
               <th>Editar</th>
               <th>Eliminar</th>
              
             </thead>
             <tbody>
             <?php $i= 1; ?>
              @if($tiendas->count() > 0)  
              
              @foreach($tiendas as $tienda)  
              <tr>
              <td>{{$i}}</td>
                <td>{{$tienda->nombre}}</td>
                <td>{{$tienda->rif}}</td>
                <td>{{$tienda->descripcion}}</td>
                <td>{{$tienda->codigo}}</td>
              
                <td>{{$tienda->telefono}}</td>
                <td>{{$tienda->direccion}}</td>

                <td><a class="btn btn-primary btn-xs" href="{{action('TiendaController@edit', $tienda->id)}}" ><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td>
                  <form action="{{action('TiendaController@destroy', $tienda->id)}}" method="post">
                   {{csrf_field()}}
                   <input name="_method" type="hidden" value="DELETE">
 
                   <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                 </td>
               </tr>
               <?php $i= $i+1; ?>
               @endforeach 
               @else
               <tr>
              
                <td colspan="8"> <div class="alert alert-danger">No hay productos registrados !! </div></td>
               
              </tr>
              @endif
            </tbody>
 
          </table>
          {{ $tiendas->links() }}
        </div>
                
        </div>
          
        </div>
       
      <div class="col col-lg-1"></div>
	</div>
@endsection