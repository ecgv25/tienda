@extends('layouts.app')
@if(Route::current()->getName() == 'recibo_export')
@section('content_title', 'Recibo')
@else
@section('content_title', 'Resumen de la venta')
@endif
@section('content')

			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Revise los campos obligatorios.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif
<div class="row justify-content-md-center">
      
        <div class="col-md-18">

			<div class="card">
			
				<div class="card-body">	
					
					<div class="table-container">
            			<table id="mytable" class="table table-borderless"  >
						  <tr><td  align="center" colspan ='4' ><b><h1>Tienda Petro</h1><b></td></tr>
						  <tr><td>{{$venta->created_at}} </td><td></td><td>00000{{$venta->id}} </td></tr> 
						  <tr><td colspan ='4'><hr></td></tr>
						  <tr><td align="center" colspan ='4'>Datos del Consumidor</td></tr>
						  <tr><td colspan ='4'>Nombre: {{$venta->comprador}}</td></tr>
						  <tr><td colspan ='4'>Cedula: {{ $venta->cedula}} 
</td></tr>
						  <tr><td colspan ='4'>Email: {{$venta->correo}} </td></tr>
						  <tr><td colspan ='4'><hr></td></tr>
						  <tr>
							<td>Articulo</td>
							<td>Descrip.</td>		
							<td>Cant</td>	
							<td>C.Unitario</td>
							</tr>

						  @foreach($productos as $prod)
						 


							<tr>
							<td>{{ $prod->productos->id }}</td>
							<td>{{ $prod->productos->nombre }}</td><td>{{ $prod->cantidad }}</td><td>{{ $prod->montoUnitario }}</td>
								
							
							</tr>
    					  @endforeach
						  <tr><td colspan ='4'><hr></td></tr>
						  <tr><td>Monto Total ({{ucfirst(trans("$venta->moneda"))}}s)</td><td></td><td>{{$venta->montoTotal}} </td></tr>
						  <tr><td colspan ='4'><hr></td></tr>
						</table>

						@if(Route::current()->getName() == 'recibo_export')
						<div class="form-group m-t-20">
							<div class="col-12">
								<a href="{{ route('mail_recibo', $venta->id) }}" class="btn btn-outline-info" >Enviar por Email</a>
							</div>
						</div>
						@endif
					
						 
						</div>	
					</div>
				</div>
				</div>
				</div>
				</div>				

	@endsection