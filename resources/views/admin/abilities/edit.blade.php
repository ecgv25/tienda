@extends('layouts.app')

@section('htmlheader_title', 'Gesitón de usuarios')

@section('content_title', 'Gesitón de habilidades')

@section('content')
	
	<div class="row justify-content-md-center">
		<div class="col col-lg-1"></div>
		<div class="col col-md-8">
			@if (session('status'))
	        <div class="alert alert-info" role="alert">
	            <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
	            {{ session('status') }}
	        </div>
	        @endif

			<div class="card">
				{{ Form::open(['route' => ['admin_abilidades_update', encrypt($ability->id)], 'method' => 'PUT', 'class' => 'card-body floating-labels']) }}
					<h4 class="card-title">
	                	Editar habilidad
	                </h4>
	                
                    <div class="form-group m-t-40{{ $errors->has('name') ? ' has-danger has-error' : '' }}">
                        <input id="name" type="text" class="text-capitalize form-control{{ $errors->has('name') ? ' form-control-danger' : '' }}" name="name" value="{{ old('name', $ability->title) }}" required autofocus>
                        <span class="bar"></span>
                        <label for="name">Nombre</label>

                        @if ($errors->has('name'))
                        <div class="form-control-feedback">
                            <small>{{ $errors->first('name') }}</small>
                        </div>
                        @endif
                    </div>

	                <div class="form-group m-t-40">
                        <div class="btn-group">
                            <a href="{{ route('admin_habilidades_index') }}" class="btn btn-outline-secondary btn-rounded">
                                Regresar
                            </a>
                            <button type="submit" class="btn btn-outline-info btn-rounded">
                                Registrar
                            </button>
                        </div>
                    </div>
         
				{{ Form::close() }}
			</div>
		</div>
		<div class="col col-lg-1"></div>
	</div>
@stop