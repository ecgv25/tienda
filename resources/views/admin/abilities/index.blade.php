@extends('layouts.app')

@section('htmlheader_title', 'Gesitón de usuarios')

@section('content_title', 'Gesitón de habilidades')

@section('content')
	
	<div class="row justify-content-md-center">
		<div class="col col-lg-1"></div>
		<div class="col col-md-8">
			@if (session('status'))
	        <div class="alert alert-info" role="alert">
	            <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
	            {{ session('status') }}
	        </div>
	        @endif

			<div class="card">
				<div class="card-body">
					<h4 class="card-title">
	                	Listado de habilidades
	                </h4>
	                @if ($abilities->count() > 0)
	                <div class="table-responsive m-t-30">
	                    <table class="table">
	                        <thead>
	                            <tr>
	                                <th>Nombre</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach ($abilities as $ability)
	                            <tr>
	                                <td>
	                                	@can('eliminar_habilidad')
	                                	<a href="{{ route('admin.habilidades.destroy', [encrypt($ability->id)]) }}" class="float-right text-danger m-r-5">
	                                		<i class="fas fa-trash"></i>
	                                	</a>
	                                	@endcan
										<a href="{{ route('admin_habilidades_edit', [encrypt($ability->id)]) }}" class="float-right m-r-5">
	                                		<i class="fas fa-edit"></i>
	                                	</a>
	                                	@can('editar_habilidad')
	                                	<a href="{{ route('admin.habilidades.edit', [encrypt($ability->id)]) }}" class="float-right m-r-5">
	                                		<i class="fas fa-edit"></i>
	                                	</a>
	                                	@endcan
	                                	
	                                    {{ $ability->name }}
	                                </td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                </div>
	                @else
	                <div class="alert alert-danger">
	                	No se encontraron habilidades registradas.
	                </div>
	                @endif

	             
	                <div class="form-group m-t-40">
                        <a href="{{ route('admin.habilidades.create') }}" class="btn btn-outline-info">
                            Agregar habilidad
                        </a>
	                </div>
	           
				</div>
			</div>
		</div>
		<div class="col col-lg-1"></div>		
	</div>
@stop