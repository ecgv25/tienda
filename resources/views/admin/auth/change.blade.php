@extends('layouts.auth')

@section('content')

    <div class="card-body">
        @include('auth.partials.logo')

        <form class="floating-labels m-t-40" action="{{ route('admin.password.change') }}" method="POST">
            <input type="hidden" name="_method" value="PUT">
            @csrf

            <div class="form-group m-t-40">
                <div class="col-xs-12">
                    <h3>{{ __('Must Change Password') }}</h3>
                    <p class="text-muted text-justify">
                        Por su seguridad, le pedimos que cambie su contraseña temporal por una de su preferencia.
                    </p>
                </div>
            </div>
            
            <div class="form-group m-t-30{{ $errors->has('password') ? ' has-error has-danger' : '' }}">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' form-control-danger' : '' }}" name="password" required>
                <span class="bar"></span>
                <label for="password">{{ __('Password') }}</label>
                
                @if ($errors->has('password'))
                <div class="form-control-feedback">
                    <small>{{ $errors->first('password') }}</small>
                </div>
                @endif
            </div>
            <div class="form-group">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                <span class="bar"></span>
                <label for="password-confirm">{{ __('Confirm Password') }}</label>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">
                        {{ __('Change Password') }}
                    </button>
                </div>
            </div>
            @captcha
        </form>
    </div>
@endsection
