@extends('layouts.auth')

@section('content')

    <div class="card-body">
        <form class="floating-labels m-t-40" id="loginform" action="{{ route('admin.login') }}" method="POST">
            @csrf

            @include('auth.partials.logo')

            <div class="form-group m-t-40{{ $errors->has('correo') ? ' has-danger has-error' : '' }}">
                <input id="correo" type="email" class="form-control{{ $errors->has('correo') ? ' form-control-danger' : '' }}" name="correo" value="{{ old('correo') }}" required autofocus>
                <span class="bar"></span>
                <label for="correo">{{ __('E-Mail Address') }}</label>

                @if ($errors->has('correo'))
                <div class="form-control-feedback">
                    <small>{{ $errors->first('correo') }}</small>
                </div>
                @endif
            </div>
            <div class="form-group">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                <span class="bar"></span>
                <label for="password">{{ __('Password') }}</label>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <div class="checkbox checkbox-primary float-left p-t-0">
                        <input class="filled-in chk-col-light-blue" type="checkbox" name="remember" id="checkbox-signup" {{ old('remember') ? 'checked' : '' }}>
                        <label for="checkbox-signup">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                    <a href="{{ route('password.request') }}" class="text-muted float-right m-t-40" style="display: none">
                        <i class="fa fa-lock m-r-5"></i>
                        {{ __('Forgot Your Password?') }}
                    </a>
                </div>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">{{ __('Login') }}</button>
                </div>
            </div>
            @captcha
        </form>
    </div>
@endsection
