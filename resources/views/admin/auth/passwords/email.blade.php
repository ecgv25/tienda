@extends('layouts.auth')

@section('content')

    <div class="card-body">
        <form class="floating-labels m-t-40" action="{{ route('password.email') }}" method="POST">
            @csrf

            @include('auth.partials.logo')

            <div class="form-group m-t-40">
                <div class="col-xs-12">
                    <h3>{{ __('Reset Password') }}</h3>
                    <p class="text-muted">Ingrese su correo electrónico y se le enviarán las instrucciones.</p>
                </div>
            </div>
            @if (session('status'))
                <div class="alert alert-success m-t-40" role="alert">
                    <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    {{ session('status') }}
                </div>
            @endif
            <div class="form-group{{ $errors->has('correo') ? ' has-error has-danger' : '' }}">
                <input id="correo" type="email" class="form-control{{ $errors->has('correo') ? ' form-control-danger' : '' }}" name="correo" value="{{ old('correo') }}" required>
                <span class="bar"></span>
                <label for="correo">{{ __('E-Mail Address') }}</label>
                
                @if ($errors->has('correo'))
                <div class="form-control-feedback">
                    <small>{{ $errors->first('correo') }}</small>
                </div>
                @endif
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">
                        {{ __('Send Password Reset Link') }}
                    </button>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <a href="{{ route('login') }}" class="text-muted float-left m-t-10">
                        <i class="fas fa-sign-in-alt m-r-5"></i>
                        {{ __('Login') }}
                    </a>
                </div>
            </div>
            @captcha
        </form>
    </div>
@endsection
