@extends('layouts.auth')

@section('content')

    <div class="card-body">
        <form class="floating-labels m-t-40" action="{{ route('password.update') }}" method="POST">
            @csrf

            <input type="hidden" name="token" value="{{ $token }}">

            @include('auth.partials.logo')

            <div class="form-group m-t-40">
                <div class="col-xs-12">
                    <h3>{{ __('Reset Password') }}</h3>
                </div>
            </div>
            @if (session('status'))
                <div class="alert alert-success m-t-40" role="alert">
                    <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    {{ session('status') }}
                </div>
            @endif
            <div class="form-group{{ $errors->has('correo') ? ' has-error has-danger' : '' }}">
                <input id="correo" type="email" class="form-control{{ $errors->has('correo') ? ' form-control-danger' : '' }}" name="correo" value="{{ old('correo') }}" required>
                <span class="bar"></span>
                <label for="correo">{{ __('E-Mail Address') }}</label>
                
                @if ($errors->has('correo'))
                <div class="form-control-feedback">
                    <small>{{ $errors->first('correo') }}</small>
                </div>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error has-danger' : '' }}">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' form-control-danger' : '' }}" name="password" required>
                <span class="bar"></span>
                <label for="password">{{ __('Password') }}</label>
                
                @if ($errors->has('password'))
                <div class="form-control-feedback">
                    <small>{{ $errors->first('password') }}</small>
                </div>
                @endif
            </div>
            <div class="form-group">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                <span class="bar"></span>
                <label for="password-confirm">{{ __('Confirm Password') }}</label>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">
                        {{ __('Reset Password') }}
                    </button>
                </div>
            </div>
            @captcha
        </form>
    </div>
@endsection
