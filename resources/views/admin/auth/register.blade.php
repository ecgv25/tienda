@extends('layouts.auth')

@section('script_additional')
    <script src="{{ cdn('/plugins/jquery-mask/jquery.mask.min.js') }}"></script>
    <script src="{{ cdn('/js/mask.init.js') }}"></script>

    <script type="text/javascript">
        var persona = $('#persona');

        persona.change(function(){

            if ($(this).val() === "natural") {
                 $('#labelcedula').html('<span  class="input-group-addon"><i class="fa fa-address-card "></i></span>'+" Cédula de identidad (V/E)");
                 $("[id*=cedula]").each(
                 function(index, value) {
                   $(this).change('V')
                 }
               );

            }
            if ($(this).val() === "juridica") {
                  $('#labelcedula').html('<span  class="input-group-addon"><i class="fa fa-id-card "></i></span>'+" Rif (J/G)");
            }

            if ($(this).val() === "Extranjero") {
                 $('#labelcedula').html('<span  class="input-group-addon"><i class="fa fa-address-book"></i></span>'+" No. de identificación");
            }
            if ($(this).val() === "juridica") {

                enableFormJuridica();
            } else {
                disableFormJuridica();
            }
        });

        if (persona.val() === "juridica") {
            enableFormJuridica();
        } else {
            disableFormJuridica();
        }

        function enableFormJuridica()
        {
            $('#legal').fadeIn(1000);
            toggleFormJuridica();
        }

        function disableFormJuridica()
        {
            $('#legal').fadeOut(1000);
            clearFormJuridica();
            toggleFormJuridica();
        }

        function clearFormJuridica()
        {
            $('#legal input, #legal select').each(function() {
                if (this.nodeName === "INPUT") {
                    $(this).val('');
                    $(this).parents('.form-group').removeClass('focused');
                }
                if (this.nodeName === "SELECT") {
                    $(this).val(0);
                }
            });
        }

        function toggleFormJuridica()
        {
            $('#legal input, #legal select').each(function() {
                if ($(this).is(':required')) {
                    $(this).removeAttr('required');
                } else {
                    $(this).attr('required', true);
                }
            });
        }
    </script>
@endsection

@section('content')

    <div class="card-body">
        <form class="floating-labels" method="POST" >
            @csrf

            @include('auth.partials.logo')

            <div class="form-group m-t-10">
                <div class="col-xs-12">
                    <b><h3>{{ __('Register') }}</h3></b>
                </div>
            </div>
            <div class="row">
                  <div class="col-md-6">
                    <div class="form-group {{ $errors->has('persona') ? ' has-danger has-error' : '' }}">
                        <select id="persona" name="persona" class="form-control{{ $errors->has('persona') ? ' form-control-danger' : '' }}">
                            <option value="natural">Natural</option>
                            <option value="juridica">Jurídica</option>
                            <option value="Extranjero">Extranjero</option>
                        </select>
                        <span class="bar"></span>
                        <label for="persona"><span  class="input-group-addon"><i class="fa fa-user "></i></span>{{ __('Person') }}</label>

                        @if ($errors->has('persona'))
                        <div class="form-control-feedback">
                            <small>{{ $errors->first('persona') }}</small>
                        </div>
                        @endif
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group {{ $errors->has('cedula') ? ' has-danger has-error' : '' }}">

                        <input id="cedula" type="text" class="form-control mask{{ $errors->has('cedula') ? ' form-control-danger' : '' }}" name="cedula" value="{{ old('cedula') }}" required autofocus>
                        <span class="bar"></span>

                        <label id=labelcedula for="cedula"><span  class="input-group-addon"><i class="fa fa-address-card "></i></span> Cédula de identidad (V/E)</label>

                        @if ($errors->has('cedula'))
                        <div class="form-control-feedback">
                            <small>{{ $errors->first('cedula') }}</small>
                        </div>
                        @endif
                    </div>
                  </div>
            </div>
            {{--<div class="row">
                <div class="col-6">
                    <div class="form-group{{ $errors->has('nombres') ? ' has-danger has-error' : '' }}">
                        <input id="nombres" type="text" class="form-control{{ $errors->has('nombres') ? ' form-control-danger' : '' }}" name="nombres" value="{{ old('nombres') }}" required autofocus>
                        <span class="bar"></span>
                        <label for="nombres">{{ __('Names') }}</label>

                        @if ($errors->has('nombres'))
                        <div class="form-control-feedback">
                            <small>{{ $errors->first('nombres') }}</small>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group{{ $errors->has('apellidos') ? ' has-danger has-error' : '' }}">
                        <input id="apellidos" type="text" class="form-control{{ $errors->has('apellidos') ? ' form-control-danger' : '' }}" name="apellidos" value="{{ old('apellidos') }}" required autofocus>
                        <span class="bar"></span>
                        <label for="apellidos">{{ __('Lastnames') }}</label>

                        @if ($errors->has('apellidos'))
                        <div class="form-control-feedback">
                            <small>{{ $errors->first('apellidos') }}</small>
                        </div>
                        @endif
                    </div>
                </div>
            </div>--}}

            <div class="form-group{{ $errors->has('correo') ? ' has-danger has-error' : '' }}">
                <input id="correo" type="email" class="form-control{{ $errors->has('correo') ? ' form-control-danger' : '' }}" name="correo" value="{{ old('correo') }}" required>
                <span class="bar"></span>
                <label for="correo"><span  class="input-group-addon"><i class="fa fa-envelope "></i></span> {{ __('E-Mail Address') }}</label>

                @if ($errors->has('correo'))
                <div class="form-control-feedback">
                    <small>{{ $errors->first('correo') }}</small>
                </div>
                @endif
            </div>
            <div class="row">
                  <div class="col-md-6">
                    <div class="form-group{{ $errors->has('password') ? ' has-error has-danger' : '' }}">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' form-control-danger' : '' }}" name="password" required>
                        <span class="bar"></span>
                        <label for="password"><span  class="input-group-addon"><i class="fa fa-lock "></i></span>{{ __('Password') }}</label>

                        @if ($errors->has('password'))
                        <div class="form-control-feedback">
                            <small>{{ $errors->first('password') }}</small>
                        </div>
                        @endif
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        <span class="bar"></span>
                        <label for="password-confirm"><span class="input-group-addon"><i class="fa fa-lock "></i></span>{{ __('Confirm Password') }}</label>
                    </div>

                  </div>
            </div>

            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-info  btn-block text-uppercase btn-rounded" type="submit">
                        {{ __('Register') }}
                    </button>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <a href="{{ route('login') }}" class="text-muted float-left">
                        <i class="fas fa-sign-in-alt m-r-5"></i>
                        {{ __('Login') }}
                    </a>
                </div>
            </div>
            @captcha
        </form>
    </div>
@endsection