@extends('layouts.app')

@section('htmlheader_title', 'Inicio')

@section('content_title', '')

@section('content')

	<!-- ============================================================== -->
    <!-- Stats box -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="m-r-20 align-self-center">
                        	<img src="{{ lv('/images/icon/expense.png') }}" alt="Income" />
                        </div>
                        <div class="align-self-center">
                            <h6 class="text-muted m-t-10 m-b-0">Total Mineros</h6>
                            <h2 class="m-t-0">
                            	{{ $mineros }}
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="m-r-20 align-self-center">
                        	<img src="{{ lv('/images/icon/income.png') }}" alt="Income" />
                        </div>
                        <div class="align-self-center">
                            <h6 class="text-muted m-t-10 m-b-0">Mineros Aprobados</h6>
                            <h2 class="m-t-0">
                            	{{ $aprobados }}
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="m-r-20 align-self-center"><img src="{{lv('/images/icon/assets.png')}}" alt="Income" /></div>
                        <div class="align-self-center">
                            <h6 class="text-muted m-t-10 m-b-0">Total Pagos</h6>
                            <h2 class="m-t-0">
                            	{{ $pagos}}
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="m-r-20 align-self-center"><img src="{{lv('/images/icon/staff.png')}}" alt="Income" /></div>
                        <div class="align-self-center">
                            <h6 class="text-muted m-t-10 m-b-0">Pagos Verificados</h6>
                            <h2 class="m-t-0">
                            	{{ $verificados }}
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
@endsection
