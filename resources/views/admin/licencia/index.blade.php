@extends('layouts.app')

@section('htmlheader_title', 'Gestión de mineros')

@section('content_title', 'Gestión de mineros')

@section('content')

    <div class="row justify-content-md-center">
        <div class="col col-lg-1"></div>
        <div class="col-md-auto">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                {{ session('status') }}
            </div>
            @endif

            <div class="card">
                <div class="card-header bg-info">
                    <div class="row">
                        <div class="col col-6">
                            <small class="text-white">Cédula de identidad</small>
                            <h6 class="text-uppercase text-white">{{ $persona->cedula }}</h6>
                        </div>
                        <div class="col col-6">
                            <small class="text-white">Nombre completo</small>
                            <h6 class="text-capitalize text-white">{{ $persona->nombre_completo }}</h6>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    @if ($persona->licencias->count()>0)
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center">Nro.</th>
                                    <th>Tipo de equipo</th>
                                    <th>Estado</th>
                                    <th>Consumo de electricidad (W)</th>
                                    <th class="text-center">Estatus</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($persona->licencias as $licencia)
                                <tr>
                                    <td class="text-center">
                                        @can('ver_licencia')
                                        <a title="Ver detalles de la solicitud" href="{{ route('admin.persona.licenses.show', [encrypt($persona->id), encrypt($licencia->id)]) }}"><strong>{{ str_pad($licencia->id, 10, "0", STR_PAD_RIGHT) }}</strong></a>
                                        @endcan

                                        @cannot('ver_licencia')
                                        <strong>{{ str_pad($licencia->id, 10, "0", STR_PAD_RIGHT) }}</strong>
                                        @endcan
                                    </td>
                                    <td class="text-uppercase">
                                        {{ $licencia->tipo->nombre }}

                                        @if ($licencia->tipo->multi_tarjeta)
                                        <span class="label label-info" title="Cantidad de tarjetas registradas: {{ $licencia->tarjetas->count() }}">
                                            {{ $licencia->tarjetas->count() }}
                                            :
                                            {{ $licencia->cantidad_tarjetas }}
                                        </span>
                                        @endif
                                    </td>
                                    <td class="text-capitalize">
                                        {{ $licencia->estado->nombre }}
                                    </td>
                                    <td class="text-center">
                                        {{ $licencia->consumo_electricidad }}
                                    </td>
                                    <td class="text-uppercase text-center small">
                                        <span class="label label-{{ $licencia->hasApprovedLicense()?'success':'danger' }}">
                                            {{ $licencia->hasApprovedLicense()?'solicitud aprobada':'aprobar solicitud' }}
                                        </span>
                                    </td>
                                    <td class="text-center">
                                        @if (! $licencia->hasApprovedLicense())
                                        <a class="btn btn-xs btn-info" href="{{ route('admin.persona.licenses.approve', [encrypt($persona->id),encrypt($licencia->id)]) }}">
                                            Aprobar
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                    <div class="alert alert-danger">
                    	Aún no ha registrado ninguna solicitud de lincecia.
                    </div>
                    @endif

                    <div class="row m-t-20">
                        <div class="col col-12">
                            <a href="{{ route('admin.persona.index', request()->all()) }}" class="btn btn-outline-secondary">
                                Regresar
                            </a>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
        <div class="col col-lg-1"></div>
        
	</div>
@endsection