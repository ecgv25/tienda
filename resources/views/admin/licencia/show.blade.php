@extends('layouts.app')

@section('htmlheader_title', 'Gestión de mineros')

@section('content_title', 'Gestión de mineros')

@section('content')

	<div class="row justify-content-md-center">
		<div class="col col-lg-1"></div>
		<div class="col col-md-8">
			<div class="card">
				<div class="card-header bg-info">
                    <div class="row">
                        <div class="col col-6">
                            <small class="text-white">Cédula de identidad</small>
                            <h6 class="text-uppercase text-white">{{ $persona->cedula }}</h6>
                        </div>
                        <div class="col col-6">
                            <small class="text-white">Nombre completo</small>
                            <h6 class="text-capitalize text-white">{{ $persona->nombre_completo }}</h6>
                        </div>
                    </div>
                </div>

				<div class="card-body">
					<div class="row">
						<div class="col col-4 b-r b-b">
							<small>Tipo de equipo</small>
							<h6 class="text-muted text-uppercase">{{ $licencia->tipo->nombre }}</h6>
						</div>
						<div class="col col-4 b-r b-b">
							<small>Moneda</small>
							<h6 class="text-muted text-uppercase">{{ $licencia->moneda->nombre }}</h6>
						</div>
						<div class="col col-4 b-b">
							<small>Pool</small>
							<h6 class="text-muted text-lowercase">{{ $licencia->moneda->pool }}</h6>
						</div>
					</div>
					@if ($licencia->tipo->multi_tarjeta)
						<div class="row">
							<div class="col col-2 b-r">
								<small>Marca</small>
							</div>
							<div class="col col-2 b-r">
								<small>Modelo</small>
							</div>
							<div class="col col-3 b-r">
								<small>Serial</small>
							</div>
							<div class="col col-3 b-r">
								<small>Hash (MH/s)</small>
							</div>
							<div class="col col-2">
								<small>Memoria</small>
							</div>
						</div>
						@foreach ($licencia->tarjetas as $tarjeta)
						<div class="row">
							<div class="col col-2 b-r">
								<h6 class="text-muted text-capitalize">{{ $tarjeta->marca->nombre }}</h6>
							</div>
							<div class="col col-2 b-r">
								<h6 class="text-muted text-capitalize">{{ $tarjeta->modelo->nombre }}</h6>
							</div>
							<div class="col col-3 b-r">
								<h6 class="text-muted text-uppercase">{{ $tarjeta->serial }}</h6>
							</div>
							<div class="col col-3 b-r">
								<h6 class="text-muted text-uppercase">{{ $tarjeta->hash }}</h6>
							</div>
							<div class="col col-2">
								<h6 class="text-muted text-uppercase">{{ $tarjeta->memoria }}</h6>
							</div>
						</div>
						@endforeach
					@else
						<div class="row">
							<div class="col col-3 b-r">
								<small>Marca</small>
								<h6 class="text-muted text-capitalize">{{ $licencia->tarjetas->first()->marca->nombre }}</h6>
							</div>
							<div class="col col-3 b-r">
								<small>Modelo</small>
								<h6 class="text-muted text-capitalize">{{ $licencia->tarjetas->first()->modelo->nombre }}</h6>
							</div>
							<div class="col col-3 b-r">
								<small>Serial</small>
								<h6 class="text-muted text-uppercase">{{ $licencia->tarjetas->first()->serial }}</h6>
							</div>
							<div class="col col-3">
								<small>Hash (GH/s)</small>
								<h6 class="text-muted text-uppercase">{{ $licencia->tarjetas->first()->hash }}</h6>
							</div>
						</div>
					@endif
					<div class="row">
						<div class="col col-12 b-b b-t">
							<small>Consumo apróximado de electricidad (W)</small>
							<h6 class="text-muted text-uppercase">{{ $licencia->consumo_electricidad }}</h6>
						</div>
					</div>

					<h4 class="card-title m-t-20">Dirección</h4>
					<div class="row">
						<div class="col col-4 b-r b-b">
							<small>Estado</small>
							<h6 class="text-muted text-capitalize">{{ $licencia->estado->nombre }}</h6>
						</div>
						<div class="col col-4 b-r b-b">
							<small>Municipio</small>
							<h6 class="text-muted text-capitalize">{{ $licencia->municipio->nombre }}</h6>
						</div>
						<div class="col col-4 b-b">
							<small>Parroquia</small>
							<h6 class="text-muted text-capitalize">{{ $licencia->parroquia->nombre }}</h6>
						</div>
					</div>
					<div class="row">
						<div class="col col-4 b-r">
							<small>Dirección</small>
							<h6 class="text-muted text-capitalize">{{ $licencia->direccion }}</h6>
						</div>
						<div class="col col-4 b-r">
							<small>Coordenadas UTM</small>
							<h6 class="text-muted text-capitalize">{{ $licencia->coordenadas }}</h6>
						</div>
						<div class="col col-4">
							<small>Observación</small>
							<h6 class="text-muted text-capitalize">{{ $licencia->observacion }}</h6>
						</div>
					</div>

					<div class="row m-t-20">
						<div class="col col-12">
							<a href="{{ route('admin.persona.licenses', [encrypt($persona->id)]) }}" class="btn btn-outline-secondary">
								Regresar
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col col-lg-1"></div>		
	</div>
@endsection