@extends('layouts.app')

@section('htmlheader_title', 'Pago por licencia')

@section('content_title', 'Pago por licencia')

@section('script_additional')
    
    <script type="text/javascript">
        // MAterial Date picker    
        $('#mdate').bootstrapMaterialDatePicker({
            lang : 'es',
            weekStart: 0,
            time: false,
            cancelText: "Cancelar",
            okText: "Aceptar",
            maxDate: new Date,
        });
    </script>
@endsection

@section('content')

	<div class="row">
        {{-- Column 1 / Información de billeteras --}}
        <div class="col col-xlg-5 col-lg-5 col-md-8 col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col col-6 text-center b-r">
                            <small>Licencias solicitadas:</small>
                            <br>
                            <h4>{{ auth()->user()->licencias()->where('approved_at', null)->count() }}</h4>
                        </div>
                        <div class="col col-6 text-center">
                            <small>Total a pagar:</small>
                            <h4>{{ auth()->user()->licencias()->where('approved_at', null)->count() }} Petros</h4>
                            <small class="text-muted">
                                (1 Petro por licencia)
                            </small>
                        </div>
                    </div>
                    @forelse ($wallets as $wallet)
                    <div class="row">
                        <div class="col col-12 b-t p-10">
                            <h5 class="card-title text-center">
                                Información de billeteras disponibles para el pago
                            </h5>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col col-8 p-10 b-t">
                            {{ $wallet->direccion }}
                        </div>
                        <div class="col col-4 p-10 b-t">
                            <img src="{{ cdn('/images/wallets/'.$wallet->qr)}}" width="75" height="75">
                        </div>
                    </div>
                    @empty
                    <div class="row">
                        <div class="col col-12 text-danger">
                            No hay billeteras disponibles en este momento, intente de nuevo más tarde.
                        </div>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>

        {{-- Column 2 / Registrar pago --}}
        <div class="col col-xlg-6 col-lg-7 col-md-8 col-12">
            @if(session('status'))
            <div class="alert alert-info">
                {{ session('status') }}
            </div>
            @endif
            
    		<div class="card">
    			<div class="card-body">
                    <form class="" method="POST" action="{{ route('pago.store') }}" enctype="multipart/form-data">
                        @csrf

                        <h4 class="card-title">
                        	Registrar pago
                        </h4>

                        <div class="floating-labels">
                            <div class="form-group m-t-40{{ $errors->has('fecha_pago') ? ' has-danger has-error' : '' }}">
                                <input type="text" class="form-control{{ $errors->has('fecha_pago') ? ' form-control-danger' : '' }}" id="mdate" name="fecha_pago" value="{{ old('fecha_pago') }}" required>
                                <span class="bar"></span>
                                <label>Fecha del pago</label>

                                @if ($errors->has('fecha_pago'))
                                <div class="form-control-feedback">
                                    <small>{{ $errors->first('fecha_pago') }}</small>
                                </div>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('concepto') ? ' has-danger has-error' : '' }}">
                                <input id="concepto" type="concepto" class="form-control{{ $errors->has('concepto') ? ' form-control-danger' : '' }}" name="concepto" value="{{ old('concepto') }}" required>
                                <span class="bar"></span>
                                <label for="concepto">Concepto</label>

                                @if ($errors->has('concepto'))
                                <div class="form-control-feedback">
                                    <small>{{ $errors->first('concepto') }}</small>
                                </div>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('hash') ? ' has-danger has-error' : '' }}">
                                <input id="hash" type="hash" class="form-control{{ $errors->has('hash') ? ' form-control-danger' : '' }}" name="hash" value="{{ old('hash') }}" required>
                                <span class="bar"></span>
                                <label for="hash">Hash de transferencia</label>

                                @if ($errors->has('hash'))
                                <div class="form-control-feedback">
                                    <small>{{ $errors->first('hash') }}</small>
                                </div>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('id_billetera') ? ' has-danger has-error' : '' }}">
                                {{ Form::select('id_billetera', $billeteras, old('id_billetera'), ['class' => 'form-control text-capitalize' . ($errors->has('id_billetera')?' form-control-danger':'')]) }}
                                <span class="bar"></span>
                                <label for="id_billetera">Seleccione la billetera destino</label>

                                @if ($errors->has('id_billetera'))
                                <div class="form-control-feedback">
                                    <small>{{ $errors->first('id_billetera') }}</small>
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-control-line">
                            <div class="form-group{{ $errors->has('comprobante') ? ' has-danger has-error' : '' }}">
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Examinar archivo</span>
                                        <span class="fileinput-exists">Cambiar</span>
                                        <input type="file" name="comprobante" id="comprobante" value="{{ old('comprobante') }}">
                                    </span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                                </div>

                                @if ($errors->has('comprobante'))
                                <div class="form-control-feedback">
                                    <small>{{ $errors->first('comprobante') }}</small>
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group m-t-40">
                            <div class="btn-group">
                                <a href="{{ route('pago.index') }}" class="btn btn-outline-secondary btn-rounded">
                                    Regresar
                                </a>
                                <button type="submit" class="btn btn-outline-info btn-rounded">
                                    Registrar
                                </button>
                            </div>
                        </div>
                        @captcha
                    </form>
                </div>
    		</div>
        </div>
	</div>
@endsection