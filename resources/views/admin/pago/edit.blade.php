@extends('layouts.app')

@section('htmlheader_title', 'Solicitud de licencias')

@section('content_title', 'Solicitud de licencias')

@section('script_additional')

    <script type="text/javascript">

        $('select[name="id_tipo_equipo"]').change(function() {
            var el = $('option:selected', $(this));

            if (el.text() === "gpu") {
                enableFormGpu();
            } else {
                disableFormGpu();
            }
        });

        function enableFormGpu() {
            $('.morecard').fadeIn(1000);
            $('.onecard').fadeOut(1000);
            $('.onecard input, .onecard select').each(function(e){
                if (this.nodeName === "INPUT") {
                    $(this).val('');
                    $(this).parents('.form-group').removeClass('focused');
                    $(this).removeAttr('required');
                }
                if (this.nodeName === "SELECT") {
                    $(this).val(0);
                }
            });
            $('.morecard').attr("required", true);
        }

        function disableFormGpu() {
            $('.onecard').fadeIn(1000);
            $('.morecard').fadeOut(1000);
            $('.morecard input, .onecard select').each(function(e){
                if (this.nodeName === "INPUT") {
                    $(this).val('');
                    $(this).parents('.form-group').removeClass('focused');
                    $(this).removeAttr('required');
                }
                if (this.nodeName === "SELECT") {
                    $(this).val(0);
                }
            });
            $('.onecard input').each(function(e){
                $(this).attr('required', true);
            });
        }

        if ($('option:selected', 'select[name="id_tipo_equipo"]').text() === "gpu") {
            enableFormGpu();
        }

        $('#btnAddTarjeta').click(function(e){
            $('#addTarjeta input[name="_form"]').val('card');
        });

        $('#addTarjeta .form-group').each(function(i){
            if ($(this).hasClass('has-error')) {
                $('#addTarjeta').modal();
                return true
            }
        });
    </script>
@endsection

@section('content')

	<div class="row">
        <div class="col-lg-6 col-xlg-5 col-md-8 col-xs-12">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                {{ session('status') }}
            </div>
            @endif

            <div class="card">
    			<div class="card-body">
                    <form class="floating-labels" method="POST" action="{{ route('licencia.update', [encrypt($licencia->id)]) }}">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">

                        <h4 class="card-title">
                        	Editar licencia
                        </h4>

                        <div class="form-group m-t-40{{ $errors->has('id_tipo_equipo') ? ' has-danger has-error' : '' }}">
                            {{ Form::select('id_tipo_equipo', $tipos, old('id_tipo_equipo', $licencia->id_tipo_equipo), ['class' => 'form-control text-capitalize' . ($errors->has('id_tipo_equipo')?' form-control-danger':'')]) }}
                            <span class="bar"></span>
                            <label for="id_tipo_equipo">Tipo de equipo</label>

                            @if ($errors->has('id_tipo_equipo'))
                            <div class="form-control-feedback">
                                <small>{{ $errors->first('id_tipo_equipo') }}</small>
                            </div>
                            @endif
                        </div>

                        <div class="morecard form-group{{ $errors->has('cantidad_tarjetas') ? ' has-danger has-error' : '' }}" style="display: none">
                            <input id="cantidad_tarjetas" type="cantidad_tarjetas" class="form-control{{ $errors->has('cantidad_tarjetas') ? ' form-control-danger' : '' }}" name="cantidad_tarjetas" value="{{ old('cantidad_tarjetas', $licencia->cantidad_tarjetas) }}">
                            <span class="bar"></span>
                            <label for="cantidad_tarjetas">Cantidad de tarjetas de video o GPU</label>

                            @if ($errors->has('cantidad_tarjetas'))
                            <div class="form-control-feedback">
                                <small>{{ $errors->first('cantidad_tarjetas') }}</small>
                            </div>
                            @endif
                        </div>

                        <div class="onecard">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group{{ $errors->has('id_marca') ? ' has-danger has-error' : '' }}">
                                        {{ Form::select('id_marca', $marcas, old('id_marca', ((!$licencia->tipo->multi_tarjeta)?$licencia->tarjetas->first()->id_marca:0)), ['class' => 'form-control text-capitalize' . ($errors->has('id_marca')?' form-control-danger':'')]) }}
                                        <span class="bar"></span>
                                        <label for="id_marca">Marca</label>

                                        @if ($errors->has('id_marca'))
                                        <div class="form-control-feedback">
                                            <small>{{ $errors->first('id_marca') }}</small>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group{{ $errors->has('id_modelo') ? ' has-danger has-error' : '' }}">
                                        {{ Form::select('id_modelo', $modelos, old('id_modelo', ((!$licencia->tipo->multi_tarjeta)?$licencia->tarjetas->first()->id_modelo:0)), ['class' => 'form-control text-capitalize' . ($errors->has('id_modelo')?' form-control-danger':'')]) }}
                                        <span class="bar"></span>
                                        <label for="id_modelo">Modelo</label>

                                        @if ($errors->has('id_modelo'))
                                        <div class="form-control-feedback">
                                            <small>{{ $errors->first('id_modelo') }}</small>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group{{ $errors->has('serial') ? ' has-danger has-error' : '' }}">
                                        <input id="serial" type="serial" class="form-control{{ $errors->has('serial') ? ' form-control-danger' : '' }}" name="serial" value="{{ old('serial', ((!$licencia->tipo->multi_tarjeta)?$licencia->tarjetas->first()->serial:'')) }}" required>
                                        <span class="bar"></span>
                                        <label for="serial">Serial</label>

                                        @if ($errors->has('serial'))
                                        <div class="form-control-feedback">
                                            <small>{{ $errors->first('serial') }}</small>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group{{ $errors->has('hash') ? ' has-danger has-error' : '' }}">
                                        <input id="hash" type="hash" class="form-control{{ $errors->has('hash') ? ' form-control-danger' : '' }}" name="hash" value="{{ old('hash', ((!$licencia->tipo->multi_tarjeta)?$licencia->tarjetas->first()->hash:'')) }}" required>
                                        <span class="bar"></span>
                                        <label for="hash">Hash (GH/s)</label>

                                        @if ($errors->has('hash'))
                                        <div class="form-control-feedback">
                                            <small>{{ $errors->first('hash') }}</small>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('consumo_electricidad') ? ' has-danger has-error' : '' }}">
                            <input id="consumo_electricidad" type="consumo_electricidad" class="form-control{{ $errors->has('consumo_electricidad') ? ' form-control-danger' : '' }}" name="consumo_electricidad" value="{{ old('consumo_electricidad', $licencia->consumo_electricidad) }}" required>
                            <span class="bar"></span>
                            <label for="consumo_electricidad">Consumo apróximado de electricidad (W)</label>

                            @if ($errors->has('consumo_electricidad'))
                            <div class="form-control-feedback">
                                <small>{{ $errors->first('consumo_electricidad') }}</small>
                            </div>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('pool') ? ' has-danger has-error' : '' }}">
                            <input id="pool" type="pool" class="form-control{{ $errors->has('pool') ? ' form-control-danger' : '' }}" name="pool" value="{{ old('pool', $licencia->pool) }}" required>
                            <span class="bar"></span>
                            <label for="pool">URL del pool</label>

                            @if ($errors->has('pool'))
                            <div class="form-control-feedback">
                                <small>{{ $errors->first('pool') }}</small>
                            </div>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('id_moneda') ? ' has-danger has-error' : '' }}">
                            {{ Form::select('id_moneda', $monedas, old('id_moneda', $licencia->id_moneda), ['class' => 'form-control text-uppercase' . ($errors->has('id_moneda')?' form-control-danger':'')]) }}
                            <span class="bar"></span>
                            <label for="id_moneda">Moneda</label>

                            @if ($errors->has('id_moneda'))
                            <div class="form-control-feedback">
                                <small>{{ $errors->first('id_moneda') }}</small>
                            </div>
                            @endif
                        </div>

                        <div class="row">
                            <div class="col-4">
                                <div class="form-group{{ $errors->has('id_estado') ? ' has-danger has-error' : '' }}">
                                    {{ Form::select('id_estado', $estados, old('id_estado', $licencia->id_estado), ['class' => 'form-control text-capitalize estado-get' . ($errors->has('id_estado')?' form-control-danger':'')]) }}
                                    <span class="bar"></span>
                                    <label for="id_estado">{{ __('State') }}</label>

                                    @if ($errors->has('id_estado'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('id_estado') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group{{ $errors->has('id_municipio') ? ' has-danger has-error' : '' }}">
                                    {{ Form::select('id_municipio', [$licencia->id_municipio => $licencia->municipio->nombre], old('id_municipio', $licencia->id_municipio), ['class' => 'form-control text-capitalize municipio-get' . ($errors->has('id_municipio')?' form-control-danger':'')]) }}
                                    <span class="bar"></span>
                                    <label for="id_municipio">{{ __('Municipality') }}</label>

                                    @if ($errors->has('id_municipio'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('id_municipio') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group{{ $errors->has('id_parroquia') ? ' has-danger has-error' : '' }}">
                                    {{ Form::select('id_parroquia', [$licencia->id_parroquia => $licencia->parroquia->nombre], old('id_parroquia', $licencia->id_parroquia), ['class' => 'form-control text-capitalize parroquia-get' . ($errors->has('id_parroquia')?' form-control-danger':'')]) }}
                                    <span class="bar"></span>
                                    <label for="id_parroquia">{{ __('Parish') }}</label>

                                    @if ($errors->has('id_parroquia'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('id_parroquia') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('direccion') ? ' has-danger has-error' : '' }}">
                            <input id="direccion" type="direccion" class="form-control{{ $errors->has('direccion') ? ' form-control-danger' : '' }}" name="direccion" value="{{ old('direccion', $licencia->direccion) }}" required>
                            <span class="bar"></span>
                            <label for="direccion">{{ __('Address') }}</label>

                            @if ($errors->has('direccion'))
                            <div class="form-control-feedback">
                                <small>{{ $errors->first('direccion') }}</small>
                            </div>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('observacion') ? ' has-danger has-error' : '' }}">
                            <input id="observacion" type="observacion" class="form-control{{ $errors->has('observacion') ? ' form-control-danger' : '' }}" name="observacion" value="{{ old('observacion', $licencia->observacion) }}">
                            <span class="bar"></span>
                            <label for="observacion">Observación</label>

                            @if ($errors->has('observacion'))
                            <div class="form-control-feedback">
                                <small>{{ $errors->first('observacion') }}</small>
                            </div>
                            @endif
                        </div>

                        <div class="form-group m-t-40">
                            <div class="col-xs-12">
                                <a href="{{ route('licencia.index') }}" class="card-link">
                                    Regresar
                                </a>
                                <button type="submit" class="btn btn-info">
                                    Actualizar
                                </button>
                            </div>
                        </div>
                        @captcha
                    </form>
                </div>
    		</div>
        </div>

        @if ($licencia->tipo->multi_tarjeta)
        <div class="col-lg-6 col-xlg-5 col-md-4 col-xs-12">
            @if ($licencia->cantidad_tarjetas > $licencia->tarjetas->count())
            <div class="alert alert-warning">
                Debe registrar todas las tarjetas del equipo para obtar a la licencia de uso.
            </div>
            @endif

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        Tarjetas del equipo
                        @if ($licencia->cantidad_tarjetas > $licencia->tarjetas->count())
                        <button id="btnAddTarjeta" class="btn btn-info btn-sm float-right" data-toggle="modal" data-target="#addTarjeta" class="model_img img-responsive">
                            Agregar
                        </button>
                        @endif
                    </h4>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Marca / Modelo</th>
                                    <th>Serial</th>
                                    <th>Hash</th>
                                    <th>Memoria</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($licencia->tarjetas as $tarjeta)
                                <tr class="small">
                                    <td class="text-capitalize">
                                        {{ $tarjeta->marca->nombre }} /
                                        {{ $tarjeta->modelo->nombre }}
                                    </td>
                                    <td class="text-uppercase">
                                        {{ $tarjeta->serial }}
                                    </td>
                                    <td class="">
                                        {{ $tarjeta->hash }}
                                    </td>
                                    <td>
                                        {{ $tarjeta->memoria }}
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="4">
                                        <span class="text-danger">Aún no ha registrado las tarjetas.</span>
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endif
	</div>

    <div id="addTarjeta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <form class="modal-content" method="POST" action="{{ route('licencia.update', [encrypt($licencia->id)]) }}">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_form" value="{{ old('_form') }}">

                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Registrar tarjeta</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body floating-labels">
                    <div class="row m-t-40">
                        <div class="col-6">
                            <div class="form-group{{ $errors->has('id_marca') ? ' has-danger has-error' : '' }}">
                                {{ Form::select('id_marca', $marcas, old('id_marca'), ['class' => 'form-control text-capitalize' . ($errors->has('id_marca')?' form-control-danger':'')]) }}
                                <span class="bar"></span>
                                <label for="id_marca">Marca</label>

                                @if ($errors->has('id_marca'))
                                <div class="form-control-feedback">
                                    <small>{{ $errors->first('id_marca') }}</small>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group{{ $errors->has('id_modelo') ? ' has-danger has-error' : '' }}">
                                {{ Form::select('id_modelo', $modelos, old('id_modelo'), ['class' => 'form-control text-capitalize' . ($errors->has('id_modelo')?' form-control-danger':'')]) }}
                                <span class="bar"></span>
                                <label for="id_modelo">Modelo</label>

                                @if ($errors->has('id_modelo'))
                                <div class="form-control-feedback">
                                    <small>{{ $errors->first('id_modelo') }}</small>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('memoria') ? ' has-danger has-error' : '' }}">
                        <input id="memoria" type="memoria" class="form-control{{ $errors->has('memoria') ? ' form-control-danger' : '' }}" name="memoria" value="{{ old('memoria') }}" required>
                        <span class="bar"></span>
                        <label for="memoria">Memoria</label>

                        @if ($errors->has('memoria'))
                        <div class="form-control-feedback">
                            <small>{{ $errors->first('memoria') }}</small>
                        </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group{{ $errors->has('serial') ? ' has-danger has-error' : '' }}">
                                <input id="serial" type="serial" class="form-control{{ $errors->has('serial') ? ' form-control-danger' : '' }}" name="serial" value="{{ old('serial') }}" required>
                                <span class="bar"></span>
                                <label for="serial">Serial</label>

                                @if ($errors->has('serial'))
                                <div class="form-control-feedback">
                                    <small>{{ $errors->first('serial') }}</small>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group{{ $errors->has('hash') ? ' has-danger has-error' : '' }}">
                                <input id="hash" type="hash" class="form-control{{ $errors->has('hash') ? ' form-control-danger' : '' }}" name="hash" value="{{ old('hash') }}" required>
                                <span class="bar"></span>
                                <label for="hash">Hash (GH/s)</label>

                                @if ($errors->has('hash'))
                                <div class="form-control-feedback">
                                    <small>{{ $errors->first('hash') }}</small>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-t-40">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-info float-right">
                                Registrar
                            </button>
                        </div>
                    </div>
                </div>
                @captcha
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection