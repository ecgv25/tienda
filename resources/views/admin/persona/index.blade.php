@extends('layouts.app')

@section('htmlheader_title', 'Gestión de mineros')

@section('content_title', 'Gestión de mineros')

@section('script_additional')

	<script type="text/javascript">
		$('#tblMinero').on('click', 'tr td.action #btnViewDocs', function(e) {
			var btn = $(this);
			btn.find('i').removeClass('fa-folder').addClass('fa-folder-open');
			$('#imgDoc').attr('src', btn.data('doc')).parent().find('span').text(btn.data('type-doc'));
			$('#imgSelfie').attr('src', btn.data('selfie'));
			$('#downloadDocs').attr('href', btn.data('download'));
			$('#approveRecord').attr('href', btn.data('approve'));
		});

		$('#btnCloseDocs').click(function(e) {
			$('#tblMinero').find('i.fa-folder-open').removeClass('fa-folder-open').addClass('fa-folder');
		});
	</script>
@endsection

@section('content')
	<div class="row justify-content-md-center">
        <div class="col col-lg-1"></div>
        <div class="col col-md-8">
			@if (session('status'))
	        <div class="alert alert-info" role="alert">
	            <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
	            {{ session('status') }}
	        </div>
	        @endif

			<div class="card">
				<div class="card-body">
	                <h4 class="card-title">
	                	Listado de mineros

	                	@if ($mineros->count() > 5)
	                	{{ Form::model(Request::all(), ['route' => 'admin.persona.index', 'method'=>'GET', 'class' => 'float-right']) }}
		                	<div class="row">
		                        <div class="col-12">
		                        	<div class="input-group">
		                        		<div class="input-group-append"></div>
		                        		{!! Form::text('termino', old('termino'), ['class' => 'form-control', 'placeholder' => 'Documento de identidad...']) !!}
				                        <div class="input-group-append">
				                            <button type="submit" class="btn btn-sm btn-outline-info">
				                                <i class="fas fa-search"></i>
				                            </button>
				                        </div>
		                        	</div>
		                        </div>
		                    </div>
                        {{ Form::close() }}
                        @endif
	                </h4>
	                @if ($mineros->count() > 0)
	                <div class="table-responsive m-t-30">
	                    <table class="table" id="tblMinero">
	                        <thead>
	                            <tr>
	                                <th>Minero</th>
	                                <th class="text-center">Licencias</th>
	                                <th class="text-center">Pagos</th>
	                                <th class="text-center">Documentos</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach ($mineros as $minero)
	                            <tr>
	                                <td>
	                                	@if (is_null($minero->empresa->id))
	                                    <span class="text-capitalize">
	                                    	@can('ver_minero')
	                                    	<a href="{{ route('admin.persona.show', [encrypt($minero->id)]) }}"><strong>{{ $minero->cedula }}</strong></a>
	                                    	@endcan

	                                    	@cannot('ver_minero')
	                                    	<strong>{{ $minero->cedula }}</strong>
	                                    	@endcan
	                                    	{{ $minero->nombre_completo }}
	                                    </span>
	                                    @else
	                                    <span class="text-capitalize">
	                                    	<a href="{{ route('admin.persona.show', [encrypt($minero->id)]) }}"><strong>{{ $minero->empresa->rif }}</strong></a>
	                                    	{{ $minero->empresa->razon_social }}
	                                    </span>
	                                    @endif
	                                    <span class="text-capitalize label label-{{ $minero->hasVerifiedId()? 'success': ($minero->waitingVerification()? 'warning': 'danger') }}">
	                                        {{ $minero->hasVerifiedId()? 'Validado': ($minero->waitingVerification()? 'Por validar': 'Registro incompleto') }}
	                                    </span>
	                                    <br>
	                                    <strong class="text-capitalize">{{ $minero->persona }}</strong>
	                                    <small class="text-muted text-lowercase">
	                                    	{{ $minero->correo }}
	                                    	@if (!is_null($minero->telefonos))
	                                    	, {{ $minero->telefonos }}
	                                    	@endif
	                                    </small>
	                                </td>
	                                <td class="text-center">
	                                	@can('licencias')
	                                	<a href="{{ route('admin.persona.licenses', [encrypt($minero->id)]) }}" title="Ver licencias registradas">
		                                	<strong class="text-success" title="Cantidad de solicitudes aprobadas">{{ $minero->licencias()->whereNotNull('approved_at')->count() }}</strong>
		                                	:
		                                	<strong class="text-warning" title="Cantidad de solicitudes registradas">{{ $minero->licencias()->whereNull('approved_at')->count() }}</strong>
	                                	</a>
                                		@endcan
	                                </td>
	                                <td class="text-center">
	                                	@can('pagos')
	                                	<a href="{{ route('admin.persona.payments', [encrypt($minero->id)]) }}" title="Ver pagos registrados">
		                                	<strong class="text-success" title="Cantidad de pagos aprobados">{{ $minero->pagos()->whereNotNull('verified_at')->count() }}</strong>
		                                	:
		                                	<strong class="text-warning" title="Cantidad de pagos notificados">{{ $minero->pagos()->whereNull('verified_at')->count() }}</strong>
		                                </a>
	                                	@endcan
	                                </td>
	                                <td class="text-center action"> 	
	                                	@can('documentos')
		                                	@if ($minero->waitingVerification())
		                                	<a id="btnViewDocs" class="waves-effect" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#viewDocuments" data-type-doc="{{ $minero->tipo_documento }}" data-doc="{{ asset('/images/persons/documentos/' . $minero->documento) }}" data-selfie="{{ asset('/images/persons/fotos/'.$minero->foto) }}" data-download="{{ route('admin.persona.download', [encrypt($minero->id)]) }}" data-approve="{{ route('admin.persona.approve', [encrypt($minero->id)]) }}" data-validated="{{ $minero->hasVerifiedId() }}">
		                                		<i class="fas fa-folder"></i>
		                                	</a>
		                                	@endif
	                                	@endcan
	                                </td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>

	                    @include('partials.footer-table', ['registros' => $mineros])
	                </div>
	                @else
	                <div class="alert alert-danger">
	                	No se encontraron mineros registrados.
	                </div>
	                @endif
	            </div>
			</div>
		</div>
		<div class="col col-lg-1"></div>
	</div>

	<div id="viewDocuments" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
        	<div class="modal-content">
        		<div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Documentos de identidad</h4>
                    <button id="btnCloseDocs" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                	<div class="row">
                		<div class="col col-12 col-md-6 b-r text-center">
                			<h5>
                				Documento
                				<small class="text-muted text-capitalize">(<span></span>)</small>
                			</h5>
                			<img id="imgDoc" class="img-fluid img-thumbnail rounded mx-auto d-block">
                		</div>
                		<div class="col col-12 col-md-6 text-center">
                			<h5>Foto</h5>
                			<img id="imgSelfie" class="img-fluid img-thumbnail rounded mx-auto d-block">
                		</div>
                	</div>
                	<div class="row m-t-20">
                		<div class="col col-12 text-center">
            				@can('aprobar_identidad')
            				<a href="" class="btn btn-outline-info btn-sm" id="approveRecord">
                				<i class="fas fa-thumbs-up"></i>
                				Aprobar identidad
                			</a>
                			@endcan

                			@can('descargar_identidad')
                			<a href="" class="btn btn-outline-info btn-sm" target="_blank" id="downloadDocs">
                				<i class="fas fa-download"></i>
                				Descargar archivos
                			</a>
                			@endcan
                		</div>
                	</div>
                </div>
        	</div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection