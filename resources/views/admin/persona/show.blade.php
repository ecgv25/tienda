@extends('layouts.app')

@section('htmlheader_title', 'Gestión de mineros')

@section('content_title', 'Gestión de mineros')

@section('content')

	<div class="row justify-content-md-center">
		<div class="col col-lg-1"></div>
		<div class="col col-md-8">
			<div class="card">
				<div class="card-header bg-info">
					<h4 class="card-title text-white">
						Persona {{ ($persona->persona) }}
					</h4>
					<p class="text-white">
						<i class="fas fa-certificate"></i>
						Cantidad de solicitud de licencias
						<span class="label label-white text-info">
							{{ $persona->licencias()->count() }}
						</span>
					</p>
					<p class="text-white">
						<i class="fas fa-credit-card"></i>
						Cantidad de pagos registrados
						<span class="label label-white text-info">
							{{ $persona->pagos()->count() }}
						</span>
					</p>
				</div>
				<div class="card-body">
					@if ($persona->isLegalEntity())
					<div class="row">
						<div class="col col-3 b-r b-b">
							<small>Rif</small>
							<h6 class="text-muted text-uppercase">{{ $persona->empresa->rif }}</h6>
						</div>
						<div class="col col-3 b-r b-b">
							<small>Razón social</small>
							<h6 class="text-muted text-capitalize">{{ $persona->empresa->razon_social }}</h6>
						</div>
						<div class="col col-3 b-r b-b">
							<small>Correo electrónico</small>
							<h6 class="text-muted text-lowercase">{{ $persona->empresa->correo }}</h6>
						</div>
						<div class="col col-3 b-b">
							<small>Teléfono fijo</small>
							<h6 class="text-muted">{{ $persona->empresa->telefono_fijo }}</h6>
						</div>
					</div>

					<h4 class="card-title m-t-20">Dirección</h4>

					<div class="row">
						<div class="col-6 b-r b-b">
							<small>{{__('State')}}</small>
							<h6 class="text-muted text-capitalize">{{ $persona->empresa->estado->nombre }}</h6>
							<small>{{__('Municipality')}}</small>
							<h6 class="text-muted text-capitalize">{{ $persona->empresa->municipio->nombre }}</h6>
							<small>{{__('Parish')}}</small>
							<h6 class="text-muted text-capitalize">{{ $persona->empresa->parroquia->nombre }}</h6>
						</div>
						<div class="col-6 b-b">
							<small>{{__('Street / Avenue')}}</small>
							<h6 class="text-muted text-capitalize">{{ $persona->empresa->calle_avenida }}</h6>
							<small>{{__('Sector / Urbanization')}}</small>
							<h6 class="text-muted text-capitalize">{{ $persona->empresa->sector_urbanizacion }}</h6>
							<small>{{__('House / Apartment')}}</small>
							<h6 class="text-muted text-capitalize">{{ $persona->empresa->casa_apto }}</h6>
						</div>
					</div>

					<h4 class="card-title m-t-20">Representante legal</h4>
					@endif

					<div class="row">
						<div class="col col-3 b-r b-b">
							<small>Cédula de identidad</small>
							<h6 class="text-muted text-uppercase">{{ $persona->cedula }}</h6>
						</div>
						<div class="col col-3 b-r b-b">
							<small>Nombre completo</small>
							<h6 class="text-muted text-capitalize">{{ $persona->nombre_completo }}</h6>
						</div>
						<div class="col col-3 b-r b-b">
							<small>Correo electrónico</small>
							<h6 class="text-muted text-lowercase">{{ $persona->correo }}</h6>
						</div>
						<div class="col col-3 b-b">
							<small>Teléfonos</small>
							<h6 class="text-muted">{{ $persona->telefonos }}</h6>
						</div>
					</div>

					@if (! $persona->isLegalEntity())
					<h4 class="card-title m-t-20">Dirección</h4>
					<div class="row">
						<div class="col-6 b-r">
							<small>{{__('State')}}</small>
							<h6 class="text-muted text-capitalize">{{ $persona->estado->nombre }}</h6>
							<small>{{__('Municipality')}}</small>
							<h6 class="text-muted text-capitalize">{{ $persona->municipio->nombre }}</h6>
							<small>{{__('Parish')}}</small>
							<h6 class="text-muted text-capitalize">{{ $persona->parroquia->nombre }}</h6>
						</div>
						<div class="col-6">
							<small>{{__('Street / Avenue')}}</small>
							<h6 class="text-muted text-capitalize">{{ $persona->calle_avenida }}</h6>
							<small>{{__('Sector / Urbanization')}}</small>
							<h6 class="text-muted text-capitalize">{{ $persona->sector_urbanizacion }}</h6>
							<small>{{__('House / Apartment')}}</small>
							<h6 class="text-muted text-capitalize">{{ $persona->casa_apto }}</h6>
						</div>
					</div>
					@endif

					<div class="row m-t-20">
						<div class="col col-12">
							<a href="{{ route('admin.persona.index', request()->all()) }}" class="btn btn-outline-secondary">
								Regresar
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col col-lg-1"></div>		
	</div>
@endsection