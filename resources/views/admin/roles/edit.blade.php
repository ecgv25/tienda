@extends('layouts.app')

@section('htmlheader_title', 'Gestión de usuarios')

@section('content_title', 'Gestión de Perfiles')

@section('script_additional')
	<script type="text/javascript">
		$('select[name="abilities[]"').select2();
	</script>
@endsection

@section('content')
	
	<div class="row justify-content-md-center">
		<div class="col col-lg-1"></div>
		<div class="col col-md-8">
			@if (session('status'))
	        <div class="alert alert-info" role="alert">
	            <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
	            {{ session('status') }}
	        </div>
	        @endif

			<div class="card">
				{{ Form::open(['route' => ['admin_perfiles_update', encrypt($role->id)], 'method' => 'PUT', 'class' => 'card-body floating-labels']) }}
					<h4 class="card-title">
	                	Editar perfil
	                </h4>
	                
                    <div class="form-group m-t-40{{ $errors->has('name') ? ' has-danger has-error' : '' }}">
                        <input id="name" type="text" class="text-capitalize form-control{{ $errors->has('name') ? ' form-control-danger' : '' }}" name="name" value="{{ old('name', $role->name) }}" required autofocus>
                        <span class="bar"></span>
                        <label for="name">Nombre</label>

                        @if ($errors->has('name'))
                        <div class="form-control-feedback">
                            <small>{{ $errors->first('name') }}</small>
                        </div>
                        @endif
                    </div>

                    <h5 for="abilities[]">Habilidades</h5>
                    <div class="form-group{{ $errors->has('abilities[]') ? ' has-danger has-error' : '' }}">
                        {!! Form::select('abilities[]', $abilities, old('abilities', $role->getAbilities()->pluck('name', 'name')), ['class' => 'form-control', 'multiple' => 'multiple', 'style' => 'width: 100%', 'data-placeholder' => 'Elegir']) !!}
                        <span class="bar"></span>

                        @if ($errors->has('abilities[]'))
                        <div class="form-control-feedback">
                            <small>{{ $errors->first('abilities[]') }}</small>
                        </div>
                        @endif
                    </div>


	                <div class="form-group m-t-40">
                        <div class="btn-group">
                            <a href="{{ route('admin_roles_index') }}" class="btn btn-outline-secondary btn-rounded">
                                Regresar
                            </a>
                            <button type="submit" class="btn btn-outline-info btn-rounded">
                                Registrar
                            </button>
                        </div>
                    </div>
                    @captcha
				{{ Form::close() }}
			</div>
		</div>
		<div class="col col-lg-1"></div>
	</div>
@stop