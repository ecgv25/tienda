@extends('layouts.app')

@section('htmlheader_title', 'Gestión de usuarios')

@section('content_title', 'Gestión de perfiles')

@section('content')
	
	<div class="row justify-content-md-center">
		<div class="col col-lg-1"></div>
		<div class="col col-md-8">
			@if (session('status'))
	        <div class="alert alert-info" role="alert">
	            <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
	            {{ session('status') }}
	        </div>
	        @endif

			<div class="card">
				<div class="card-body">
					<h4 class="card-title">
	                	Listado de perfiles
	                </h4>
	                @if ($roles->count() > 0)
	                <div class="table-responsive m-t-30">
	                    <table class="table">
	                        <thead>
	                            <tr>
	                                <th width="30%">Nombre</th>
	                                <th>Habilidades</th>
	                                <th width="10%"></th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach ($roles as $role)
	                            <tr>
	                                <td class="text-capitalize">{{ $role->name }}</td>
	                                <td>
	                                    @foreach ($role->abilities()->pluck('name') as $ability)
	                                        <span class="label label-info">{{ $ability }}</span>
	                                    @endforeach
	                                </td>
	                                <td class="text-center">
									<a href="{{ route('admin_roles_edit', [encrypt($role->id)]) }}" class="m-r-5">
	                                		<i class="fas fa-edit"></i>
	                                	</a>
	                                	@can('editar_perfil')
	                                	<a href="{{ route('admin.perfiles.edit', [encrypt($role->id)]) }}" class="m-r-5">
	                                		<i class="fas fa-edit"></i>
	                                	</a>
	                                	@endcan

	                                	@can('eliminar_perfil')
	                                	<a class="text-danger m-r-5"
                                            href="{{ route('admin.perfiles.destroy', [encrypt($role->id)]) }}"
                                            data-method="delete"
                                            data-token="{{ csrf_token() }}"
                                            data-confirm="Esta seguro? No podrá recuperar el registro.">
                                            <i class="fas fa-trash"></i>
                                        </a>
	                                	@endcan
	                                </td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                </div>
	                @else
	                <div class="alert alert-danger">
	                	No se encontraron perfiles registradas.
	                </div>
	                @endif
					<div class="form-group m-t-40">
                        <a href="{{ route('admin_perfiles_create') }}" class="btn btn-outline-info">
                            Agregar perfil
                        </a>
	                </div>
	                @can('crear_perfil')
	                <div class="form-group m-t-40">
                        <a href="{{ route('admin.perfiles.create') }}" class="btn btn-outline-info">
                            Agregar perfil
                        </a>
	                </div>
	                @endcan
				</div>
			</div>
		</div>
		<div class="col col-lg-1"></div>		
	</div>
@stop