@extends('layouts.app')

@section('htmlheader_title', 'Solicitudes de Licencia')

@section('content_title', 'Solicitudes de Licencia')

@section('script_additional')

	<script type="text/javascript">
		$('#tblMinero').on('click', 'tr td.action #btnViewDocs', function(e) {
			var btn = $(this);
			btn.find('i').removeClass('fa-folder').addClass('fa-folder-open');
			$('#imgDoc').attr('src', btn.data('doc')).parent().find('span').text(btn.data('type-doc'));
			$('#imgSelfie').attr('src', btn.data('selfie'));
			$('#downloadDocs').attr('href', btn.data('download'));
			$('#approveRecord').attr('href', btn.data('approve'));
		});

		$('#btnCloseDocs').click(function(e) {
			$('#tblMinero').find('i.fa-folder-open').removeClass('fa-folder-open').addClass('fa-folder');
		});
	</script>
@endsection

@section('content')
	<div class="row justify-content-md-center">
        <div class="col col-lg-1"></div>
        <div class="col col-md-8">
			@if (session('status'))
	        <div class="alert alert-info" role="alert">
	            <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
	            {{ session('status') }}
	        </div>
	        @endif

			<div class="card">
				<div class="card-body">
	                <h4 class="card-title">
	                	Listado de solicitudes de licencias de casa de intercambio

	                	@if ($mineros->count() > 5)
	                	{{ Form::model(Request::all(), ['route' => 'admin.persona.index', 'method'=>'GET', 'class' => 'float-right']) }}
		                	<div class="row">
		                        <div class="col-12">
		                        	<div class="input-group">
		                        		<div class="input-group-append"></div>
		                        		{!! Form::text('termino', old('termino'), ['class' => 'form-control', 'placeholder' => 'Documento de identidad...']) !!}
				                        <div class="input-group-append">
				                            <button type="submit" class="btn btn-sm btn-outline-info">
				                                <i class="fas fa-search"></i>
				                            </button>
				                        </div>
		                        	</div>
		                        </div>
		                    </div>
		                    @captcha
                        {{ Form::close() }}
                        @endif
	                </h4>
	                @if ($mineros->count() > 0)
	                <div class="table-responsive m-t-30">
	                    <table class="table" id="tblMinero">
	                        <thead>
	                            <tr>
	                                <th>Empresa</th>
	                                <th>F. Solicitud</th>
	                                <th class="text-center">Acciones</th>	                       
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach ($mineros as $minero)
	                            <tr>
	                                <td>
	                                	
	                                    <span class="text-capitalize">
	                                    	<strong>{{ $minero->rif }}</strong>
	                                    	{{ $minero->razon_social }}
	                                    </span>	                                    
	                                    	                                    
	                                </td>	   
	                                <td>{{ $minero->f_solicitud }}</td>                             
	                                <td>
	                                	@if ($minero->estatus=='S')
	                                	<a class="btn btn-info" href="solicitud_licencias/<?php echo encrypt($minero->id)?>/aprobarsolicitud"> Aprobar</a>
	                                	@else
	                                	<span class="text-capitalize">
	                                    	<strong>Aprobada</strong>
	                                    	{{ $minero->f_estatus }}
	                                    </span>	
	                                	@endif 
	                                </td>
	                                
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>

	                    
	                </div>
	                @else
	                <div class="alert alert-danger">
	                	No se encontraron licencias registradas.
	                </div>
	                @endif
	            </div>
			</div>
		</div>
		<div class="col col-lg-1"></div>
	</div>

	
@endsection