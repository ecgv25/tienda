@extends('layouts.app')

@section('htmlheader_title', 'Gestión de usuarios')

@section('content_title', 'Gestión de usuarios')


@section('content')

	<div class="row justify-content-md-center">
        <div class="col col-lg-2"></div>
        <div class="col col-md-auto">
            <div class="card">
    			<div class="card-body">
                    <form class="floating-labels" id="usuario-form" method="POST" action="{{ route('admin.usuario.store') }}">
                        @csrf

                        <h4 class="card-title">
                        	Registrar usuario
                        </h4>

                        <div class="form-group m-t-40{{ $errors->has('cedula') ? ' has-danger has-error' : '' }}">
                            <input id="cedula" type="text" class="form-control ci-mask{{ $errors->has('cedula') ? ' form-control-danger' : '' }}" name="cedula" value="{{ old('cedula') }}" required autofocus>
                            <span class="bar"></span>
                            <label for="cedula">{{ __('Cedula') }}</label>

                            @if ($errors->has('cedula'))
                            <div class="form-control-feedback">
                                <small>{{ $errors->first('cedula') }}</small>
                            </div>
                            @endif
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group{{ $errors->has('name') ? ' has-danger has-error' : '' }}">
                                    <input id="name" type="text" class="text-capitalize form-control{{ $errors->has('name') ? ' form-control-danger' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                                    <span class="bar"></span>
                                    <label for="name">Nombre</label>

                                    @if ($errors->has('name'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('name') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group{{ $errors->has('apellidos') ? ' has-danger has-error' : '' }}">
                                    <input id="apellidos" type="text" class="text-capitalize form-control{{ $errors->has('apellidos') ? ' form-control-danger' : '' }}" name="apellidos" value="{{ old('apellidos') }}" required autofocus>
                                    <span class="bar"></span>
                                    <label for="apellidos">Apellido</label>

                                    @if ($errors->has('apellidos'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('apellidos') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group{{ $errors->has('correo') ? ' has-danger has-error' : '' }}">
                                    <input id="correo" type="email" class="form-control text-lowercase{{ $errors->has('correo') ? ' form-control-danger' : '' }}" name="correo" value="{{ old('correo') }}" required>
                                    <span class="bar"></span>
                                    <label for="correo">Correo</label>

                                    @if ($errors->has('correo'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('correo') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group{{ $errors->has('perfiles') ? ' has-danger has-error' : '' }}">
                                    {{ Form::select('perfiles', $perfiles, old('perfiles'), ['class' => 'text-capitalize form-control' . ($errors->has('perfiles')?' form-control-danger':'')]) }}
                                    <span class="bar"></span>
                                    <label for="perfiles">Rol</label>

                                    @if ($errors->has('perfiles'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('perfiles') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-6">
                                     <div class="form-group">
											<select name="tienda" id="tienda" class="form-control input-sm">
												
												@foreach ($tiendas as $tienda)
													<option value="{{ $tienda->id }}">{{ $tienda->nombre }}</option>
												@endforeach
											</select>
											<label for="nombres">Tienda</label>	
											<span class="bar"></span>
										
                                    </div>
                        </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group{{ $errors->has('telefono_movil') ? ' has-danger has-error' : '' }}">
                                    <input id="telefono_movil" type="text" class="movil-mask form-control{{ $errors->has('telefono_movil') ? ' form-control-danger' : '' }}" name="telefono_movil" value="{{ old('telefono_movil') }}" required>
                                    <span class="bar"></span>
                                    <label for="telefono_movil">Telefono Movil</label>

                                    @if ($errors->has('telefono_movil'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('telefono_movil') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group{{ $errors->has('telefono_fijo') ? ' has-danger has-error' : '' }}">
                                    <input id="telefono_fijo" type="text" class="fijo-mask form-control{{ $errors->has('telefono_fijo') ? ' form-control-danger' : '' }}" name="telefono_fijo" value="{{ old('telefono_fijo') }}">
                                    <span class="bar"></span>
                                    <label for="telefono_fijo">Telefono Fijo</label>

                                    @if ($errors->has('telefono_fijo'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('telefono_fijo') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group{{ $errors->has('cargo') ? ' has-danger has-error' : '' }}">
                                    <input id="cargo" type="text" class="text-capitalize form-control{{ $errors->has('cargo') ? ' form-control-danger' : '' }}" name="cargo" value="{{ old('cargo') }}" required>
                                    <span class="bar"></span>
                                    <label for="cargo">Cargo</label>

                                    @if ($errors->has('cargo'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('cargo') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group{{ $errors->has('unidad_administrativa') ? ' has-danger has-error' : '' }}">
                                    <input id="unidad_administrativa" type="text" class="text-capitalize form-control{{ $errors->has('unidad_administrativa') ? ' form-control-danger' : '' }}" name="unidad_administrativa" value="{{ old('unidad_administrativa') }}" required>
                                    <span class="bar"></span>
                                    <label for="unidad_administrativa">Unidad administrativa</label>

                                    @if ($errors->has('unidad_administrativa'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('unidad_administrativa') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group m-t-40">
                            <div class="btn-group">
                                <a href="{{ route('admin.usuario.index') }}" class="btn btn-outline-secondary btn-rounded">
                                    Regresar
                                </a>
                                <button type="submit" class="btn btn-outline-info btn-rounded">
                                    Registrar
                                </button>
                            </div>
                        </div>
                
                    </form>
                </div>
    		</div>
        </div>
        <div class="col col-lg-2"></div>
	</div>
@endsection