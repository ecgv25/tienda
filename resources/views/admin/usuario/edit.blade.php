@extends('layouts.app')

@section('htmlheader_title', 'Gestión de usuarios')

@section('content_title', 'Gestión de usuarios')

@section('script_additional')

    
@endsection

@section('content')

	<div class="row justify-content-md-center">
        <div class="col col-lg-2"></div>
        <div class="col col-md-auto">
            <div class="card">
    			<div class="card-body">
                    <form class="floating-labels" id="usuario-form" method="POST" action="{{ route('admin.usuario.update', [encrypt($usuario->id)]) }}">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf

                        <h4 class="card-title">
                        	Editar usuario
                        </h4>

                        <div class="row">
                            <div class="col-6 b-r b-b b-t">
                                <small class="text-muted">Cedula</small>
                                <h6 class="text-uppercase">{{ $usuario->cedula }}</h6>

                                <small class="text-muted">Correo</small>
                                <h6 class="text-lowercase">{{ $usuario->email }}</h6>
                            </div>
                            <div class="col-6 b-b b-t">
                                <small class="text-muted">Nombre</small>
                                <h6 class="text-capitalize">{{ $usuario->name}}</h6>

                               
                            </div>
                        </div>

                        <div class="row m-t-40">
                            <div class="col-6">
                                <div class="form-group{{ $errors->has('telefono_movil') ? ' has-danger has-error' : '' }}">
                                    <input id="telefono_movil" type="telefono_movil" class="movil-mask form-control{{ $errors->has('telefono_movil') ? ' form-control-danger' : '' }}" name="telefono_movil" value="{{ old('telefono_movil', $usuario->telefono_movil) }}" required>
                                    <span class="bar"></span>
                                    <label for="telefono_movil">Telefono Movil</label>

                                    @if ($errors->has('telefono_movil'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('telefono_movil') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group{{ $errors->has('telefono_fijo') ? ' has-danger has-error' : '' }}">
                                    <input id="telefono_fijo" type="telefono_fijo" class="fijo-mask form-control{{ $errors->has('telefono_fijo') ? ' form-control-danger' : '' }}" name="telefono_fijo" value="{{ old('telefono_fijo', $usuario->telefono_fijo) }}">
                                    <span class="bar"></span>
                                    <label for="telefono_fijo">telefono Fijo</label>

                                    @if ($errors->has('telefono_fijo'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('telefono_fijo') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                                     <div class="form-group">
											<select name="tienda" id="tienda" class="form-control input-sm">
												
												@foreach ($tiendas as $tienda)
													<option value="{{ $tienda->id }}">{{ $tienda->nombre }}</option>
												@endforeach
											</select>
											<label for="nombres">Tienda</label>	
											<span class="bar"></span>
										
                                    </div>
                        </div>                
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group{{ $errors->has('cargo') ? ' has-danger has-error' : '' }}">
                                    <input id="cargo" type="text" class="text-capitalize form-control{{ $errors->has('cargo') ? ' form-control-danger' : '' }}" name="cargo" value="{{ old('cargo', $usuario->cargo) }}" required>
                                    <span class="bar"></span>
                                    <label for="cargo">Cargo</label>

                                    @if ($errors->has('cargo'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('cargo') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group{{ $errors->has('unidad_administrativa') ? ' has-danger has-error' : '' }}">
                                    <input id="unidad_administrativa" type="unidad_administrativa" class="text-capitalize form-control{{ $errors->has('unidad_administrativa') ? ' form-control-danger' : '' }}" name="unidad_administrativa" value="{{ old('unidad_administrativa', $usuario->unidad_administrativa) }}" required>
                                    <span class="bar"></span>
                                    <label for="unidad_administrativa">Unidad administrativa</label>

                                    @if ($errors->has('unidad_administrativa'))
                                    <div class="form-control-feedback">
                                        <small>{{ $errors->first('unidad_administrativa') }}</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('perfiles') ? ' has-danger has-error' : '' }}">
                            {{ Form::select('perfiles', $perfiles, old('perfiles', $usuario->roles()->pluck('name', 'name')), ['class' => 'text-capitalize form-control' . ($errors->has('perfiles')?' form-control-danger':'')]) }}
                            <span class="bar"></span>
                            <label for="perfiles">Rol</label>

                            @if ($errors->has('perfiles'))
                            <div class="form-control-feedback">
                                <small>{{ $errors->first('perfiles') }}</small>
                            </div>
                            @endif
                        </div>
                        <h6 class="card-title m-t-10">
                           Habilidades de este usuario
                        </h6>

                        @foreach($abilidadesUsuario as $abilidad)  

                        <div class="col-6">
                         <span class="text-capitalize label label-primary">{{ $abilidad->name}}</span>
                         </div>
                         
                         @endforeach 
                        
                         <br> <hr> <br>
                        <h6 class="card-title m-t-10">
                            Deje estos campos en blanco si no se quiere cambiar la contraseña
                        </h6>
                        <div class="form-group m-t-30{{ $errors->has('password') ? ' has-error has-danger' : '' }}">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' form-control-danger' : '' }}" name="password">
                            <span class="bar"></span>
                            <label for="password">Clave</label>
                            
                            @if ($errors->has('password'))
                            <div class="form-control-feedback">
                                <small>{{ $errors->first('password') }}</small>
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                            <span class="bar"></span>
                            <label for="password-confirm">Confirmacion</label>
                        </div>

                        <div class="form-group m-t-40">
                            <div class="btn-group">
                                <a href="{{ route('admin.usuario.index') }}" class="btn btn-outline-secondary btn-rounded">
                                    Regresar
                                </a>
                                <button type="submit" class="btn btn-outline-info btn-rounded">
                                    Actualizar
                                </button>
                            </div>
                        </div>
                    
                    </form>
                </div>
    		</div>
        </div>
        <div class="col col-lg-2"></div>
	</div>
@endsection