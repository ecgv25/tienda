@extends('layouts.app')

@section('htmlheader_title', 'Gestión de usuarios')

@section('content_title', 'Gestión de usuarios')

@section('content')
<div class="row justify-content-md-center">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
			@if (session('status'))
	        <div class="alert alert-info" role="alert">
	            <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
	            {{ session('status') }}
	        </div>
			@endif
			 <h4 class="card-title">
	                	Listado de usuarios
	                </h4>
	                @if ($usuarios->count() > 0)
	                <div class="table-responsive m-t-30">
	                    <table class="table">
	                        <thead>
	                            <tr>
									<th>Usuario</th>
									<th>Cedula</th>
									<th>Nombre</th>
									<th>Roles</th>
									<th>Correo</th>
									<th>Cargo</th>
									<th>Unidad Administrativa</th>
									<th>Editar</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach ($usuarios as $usuario)
	                            <tr>
									<td>
									{{ $usuario->email }}	
									</td>
									<td>
										{{ $usuario->cedula }}
									</td>	
									<td>
									{{ $usuario->name }}
									</td>
									<td>
										@foreach ($usuario->roles->pluck('name') as $role)
	                                        <span class="text-capitalize label label-primary">{{ $role }}</span>
	                                    @endforeach
									</td>
	                                <td>
									{{ $usuario->email }}	
									</td>   
									<td>
									{{ $usuario->cargo }}
									</td> 
									<td>
									{{ $usuario->unidad_administrativa }}
									</td> 
									<td>
	                                	<a href="{{ route('admin.usuario.edit', [encrypt($usuario->id)]) }}" class="float-right">
	                                		<i class="fas fa-edit"></i>
	                                	</a>
									</td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                </div>
	                @else
	                <div class="alert alert-danger">
	                	No se encontraron usuarios registrados.
	                </div>
	                @endif

	            
	                <div class="form-group m-t-40">
                        <a href="{{ route('admin.usuario.create') }}" class="btn btn-outline-info">
                            Agregar usuario
                        </a>
	               
			</div>
		</div>
		<div class="col col-lg-2"></div>		
	</div>
	</div>	
@endsection