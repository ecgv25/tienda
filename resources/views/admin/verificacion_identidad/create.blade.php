@extends('layouts.app')

@section('htmlheader_title', 'Gestión de solicitudes')

@section('content_title', 'Agregar Moneda')

@section('content')

<div class="row justify-content-md-center">
         <div class="col col-md-10">
            <div class="card">
                <div class="card-body">
    
                    <h2 class="page-header">Datos para Registrar una Moneda</h2>

                    <div class="panel panel-default">                       
                        <div class="panel-body">
                                    
                            <form action="{{ route('admin.monedas.store') }}" method="POST" class="form-horizontal">
                                {{ csrf_field() }}

                               <div class="form-group m-t-40{{ $errors->has('nombre') ? ' has-danger has-error' : '' }}">
                                    <label for="nombre" class="col-sm-3 control-label">Nombre</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="nombre" id="nombre" class="form-control" required="required">
                                        @if ($errors->has('nombre'))
                                            <div class="form-control-feedback">
                                                <small>{{ $errors->first('nombre') }}</small>
                                            </div>
                                        @endif
                                    </div>
                                    
                                </div>                                                                              

                                <div class="form-group m-t-40{{ $errors->has('conversion') ? ' has-danger has-error' : '' }}">
                                    <label for="conversion" class="col-sm-6 control-label">Conversion</label>
                                    
                                    <div class="col-sm-6">
                                        <input type="text" name="conversion" id="conversion" class="form-control" required="required">
                                        <label for="conversion_help" class="help">Ingrese un número, para los decimales utilice punto (.)</label>
                                        @if ($errors->has('conversion'))
                                            <div class="form-control-feedback">
                                                <small>{{ $errors->first('conversion') }}</small>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                        
                                <div class="form-group m-t-40">
                                    <div class="btn-group">
                                        <a class="btn btn-default" href="{{ url('admin/monedas') }}" class="btn btn-outline-secondary btn-rounded">
                                            Regresar
                                        </a>
                                        <button type="submit" class="btn btn-outline-info btn-rounded">
                                            Procesar
                                        </button>
                                    </div>
                                </div>    
                                
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






@endsection