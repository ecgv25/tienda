@extends('layouts.app')

@section('htmlheader_title', 'Gestión de monedas')

@section('content_title', 'Gestión de Monedas')

@section('content')

<div class="row justify-content-md-center">
         <div class="col col-md-10">
            <div class="card">
                <div class="card-body">
    
                    <h2 class="page-header">Editar Moneda {{$model->nombre}}</h2>

                    <div class="panel panel-default">                        

                        <div class="panel-body">
                                    
                            <form action="{{ url('admin/monedas'.( isset($model) ? "/" . $model->id : "")) }}" method="POST" class="form-horizontal">
                                {{ csrf_field() }}
                                
                               <input type="hidden" name="_method" value="PATCH">

                               <div class="form-group">
                                    <label for="nombre" class="col-sm-3 control-label">Nombre</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="nombre" id="nombre" class="form-control" value="{{$model->nombre}}">
                                    </div>
                                </div>                                                                              

                                <div class="form-group">
                                    <label for="conversion" class="col-sm-3 control-label">Conversion</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="conversion" id="conversion" class="form-control" value="{{$model->conversion}}">
                                    </div>
                                </div>
                                        
                                <div class="form-group m-t-40">
                                    <div class="btn-group">
                                        <a class="btn btn-default" href="{{ url('admin/monedas') }}" class="btn btn-outline-secondary btn-rounded">
                                            Regresar
                                        </a>
                                        <button type="submit" class="btn btn-outline-info btn-rounded">
                                            Procesar
                                        </button>
                                    </div>
                                </div>    
                                
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






@endsection