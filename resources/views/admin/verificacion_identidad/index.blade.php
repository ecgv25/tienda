@extends('layouts.app')

@section('htmlheader_title', 'Gestión de verificación de identidad')

@section('content_title', 'Verificación de Identidad')

@section('script_additional')

    <script type="text/javascript">

        
    </script>
@endsection
@section('content')

<div class="row justify-content-md-center">
         <div class="col col-md-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="page-header">Listado de Personas por Verificación de Identidad</h2>

                    <div class="panel panel-default">                        
                        @if (session('status'))
                            <div class="alert alert-info" role="alert">
                                <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="alert alert-info" id="mensaje" role="alert">
                            <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                        </div>
                        <div class="panel-body">
                            <div class="">
                                <table class="table table-striped" id="thegrid">
                                  <thead>
                                    <tr>
                                        <th>Datos</th>                   
                                        <th>Ver Documento</th>
                                        <th>Aprobar</th>
                                        <th>Rechazar</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  </tbody>
                                </table>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="modal fade modalRechazar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
                        
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Motivos para Rechazar Identidad</h4>
                <button id="btnCloseRechazar" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="alert alert-danger" id="error_rechazar">                                        
            </div>
            <div class="modal-body">
                 <form id="procesar_rechazo" name="procesar_rechazo" action="{{ url('admin/no_aprobacion') }}" method="POST" class="form-horizontal form-label-left input_mask">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
                   <input type="hidden" id="id_persona" name="id_persona" class="form-control has-feedback-left" value="" >                               
                    <div class="col-12">                                                             
                        <input type="radio" id="motivo_no_legible" name="motivo" value="documento no legible" class="radio-col-light-blue" required="required" 
                        />
                        <label for="motivo_no_legible">El (los) documento(s) no legible(s)</label>
                    </div>    
                        
                    <div class="col-12">                                                             
                        <input type="radio" id="motivo_no_coinciden" name="motivo" value="datos no coinciden con anexos" class="radio-col-light-blue" 
                        />
                        <label for="motivo_no_coinciden">Los datos suministrados no coinciden con los presentados en las imagenes anexas</label>
                    </div>
                
                    <div class="col-12">                                                                     
                        <input type="radio" id="motivo_no_solicitados" name="motivo" value="los documentos no son los solicitados" class="radio-col-light-blue" 
                        />
                        <label for="motivo_no_solicitados">El(los) documento(s) no son el(los) solicitado(s)</label>
                        
                    </div>
                
                    <div class="col-12">                                                                     
                        <input type="radio" id="motivo_vencimiento" name="motivo" value="documentos vencidos" class="radio-col-light-blue" 
                        />
                        <label for="motivo_vencimiento">El (los) documento(s) anexos está(n) vencido(s)</label>
                        
                    </div>

                    <div class="modal-footer">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-3">
                            <button type="button" class="btn btn-outline-info" data-dismiss="modal">Cerrar</button>
                            <button id="guardar_rechazar" type="submit" class="btn btn-outline-info"> Guardar</button>
                        </div>
                    </div>
                    @captcha
                </form>
                
            </div>
        </div>
    </div>
</div>

<div id="viewDocuments" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Documentos de identidad</h4>
                    <button id="btnCloseDocs" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col col-12 col-md-6 b-r text-center">
                            <h5>
                                Documento
                                <small class="text-muted text-capitalize">(<span></span>)</small>
                            </h5>
                            <img id="imgDoc" class="img-fluid img-thumbnail rounded mx-auto d-block">
                        </div>
                        <div class="col col-12 col-md-6 text-center">
                            <h5>Foto</h5>
                            <img id="imgSelfie" class="img-fluid img-thumbnail rounded mx-auto d-block">
                        </div>
                    </div>
                    <div class="row m-t-20">
                        <div class="col col-12 text-center">
                            @can('descargar_identidad')
                            <a href="" class="btn btn-outline-info btn-sm" target="_blank" id="downloadDocs">
                                <i class="fas fa-download"></i>
                                Descargar archivos
                            </a>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>  

@endsection

@section('scripts')
    <script type="text/javascript">        
        $(document).ready(function(){            
           $("#mensaje").hide();
           $('.alert').fadeOut(4500);
           var theGrid = $('#thegrid').DataTable({
                "destroy": true,
                "language": {
                    "url": "/datatable_es.json"
                 },
                "paging":   false,
                "ordering": false,
                "info":     false,
                "lengthMenu": false,
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "responsive": true,                
                "ajax": "{{url('admin/verificacion_identidad/grid')}}",                
            });

            $('#btnCloseDocs').click(function(e) {            
                $('#thegrid').find('i.fa-folder-open').removeClass('fa-folder-open').addClass('fa-folder');
            });

        });        
        function viewDocs() {
            
            var btn = $("#btnViewDocs");
            btn.find('i').removeClass('fa-folder').addClass('fa-folder-open');
            $('#imgDoc').attr('src', btn.data('doc')).parent().find('span').text(btn.data('type-doc'));
            $('#imgSelfie').attr('src', btn.data('selfie'));
            $('#downloadDocs').attr('href', btn.data('download'));
        }
        function viewRechazar() {            
            var btn = $("#abrirModalRechazar");            
            $('#id_persona').attr('value', btn.data('id'));            
        }
        function aprobar(id) {            
            var theGrid = $('#thegrid').DataTable();
            $.ajax({url: '{{ url('admin/verificacion_identidad/aprobacion') }}/' + id, success: function(
            result){
                theGrid.ajax.reload();
                $("#mensaje").show();
                $("#mensaje").text('Aprobado con exito!');
            }});                
            return false;
        }
    </script>
@endsection