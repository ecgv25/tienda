@extends('layouts.app')

@section('htmlheader_title', 'Gestión de monedas')

@section('content_title', 'Gestión de Monedas')

@section('content')

<div class="row justify-content-md-center">
         <div class="col col-md-10">
            <div class="card">
                <div class="card-body">


            <h2 class="page-header text-capitalize">Detalle de la Moneda {{ $model->nombre }}</h2>

            <div class="panel panel-default">    

                <div class="panel-body">                
                            
                    <div class="form-group">
                        <label for="nombre" class="col-sm-6 control-label">Nombre</label>
                        <div class="col-sm-6 text-capitalize">
                            {{ $model->nombre }}

                        </div>
                    </div>
                            
                    <div class="form-group">
                        <label for="conversion" class="col-sm-6 control-label">Conversion</label>
                        <div class="col-sm-6">
                           {{ $model->conversion }}
                        </div>
                    </div>
                            
                    <div class="form-group">
                        <label for="listar" class="col-sm-6 control-label">Listar</label>
                        <div class="col-sm-6">
                            @if ($model->listar==1)
                                SI
                            @else 
                                NO                           
                            @endif
                        </div>
                    </div>
                            
                    <div class="form-group">
                        <label for="created_at" class="col-sm-6 control-label">Fecha de Creación</label>
                        <div class="col-sm-6">
                            {{ $model->created_at }}                
                        </div>
                    </div>
                            
                    <div class="form-group">
                        <label for="updated_at" class="col-sm-6 control-label">Fecha de Modificación</label>
                        <div class="col-sm-6">
                            {{ $model->updated_at }}                
                        </div>
                    </div>
                            
                    <div class="form-group">
                        <label for="deleted_at" class="col-sm-6 control-label">Fecha de Eliminación</label>
                        <div class="col-sm-6">
                            {{ $model->deleted_at }}                
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <a class="btn btn-default" href="{{ url('admin/monedas') }}"><i class="glyphicon glyphicon-chevron-left"></i>Regresar</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>







@endsection