@extends('layouts.app')

@section('htmlheader_title', 'Inicio')

@section('content_title', '')

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            Bienvenido a la Petro Tienda
        </h3>
    </div>
    <div class="row"> 
            <div class="col col-12 col-sm-12 col-md-12 col-lg-12"> 
                        <div class="panel panel-default">
                            <div class="panel-body">
                            <div>
                                @if($tienda)
                               <div class="col col-2 col-sm-2 col-md-2 col-lg-2"> 
                                <p ><h3 class="text-themecolor">{{$tienda->nombre}}</h3></p>
                                
                                <p><img src="{{('/images/qr.png') }}" alt="Inicio" width="180" height="180" /></p>
                              
                                </div>
                                <div class="col col-4 col-sm-4 col-md-4 col-lg-4"> 
                                <br><br>
                                <p>RIF: {{$tienda->rif}}</p>
                                <p>Direccion: {{$tienda->direccion}}</p>
                                <p>Telefonos: {{$tienda->telefono}}</p>
                              
                                <p>Codigo Hash: {{$tienda->codigoHash}}</p>
                                </div>
                                @endif
                                <div class="col col-6 col-sm-6 col-md-6 col-lg-6"> 
                                <br><br> <br>
                                <table class="table table-borderless">
                                <tr>
                                <td><p ><h3 class="text-themecolor"><b>BALANCE</b></h3></p></td>
                                <td><img src="{{('/images/BITCOIN_PEQUEÑO.png') }}" alt="Inicio" width="150" height="80" /></td>
                                <td><img src="{{('/images/DASH_PEQUEÑO.png') }}" alt="Inicio" width="150" height="80" /></td>
                                </tr>
                                <tr>
                                <td><img src="{{('/images/ETHEREUM_PEQUEÑO.png') }}" alt="Inicio" width="150" height="80" /></td>
                                <td><img src="{{('/images/LITECOIN_PEQUEÑO.png') }}" alt="Inicio" width="150" height="80" /></td>
                                <td><img src="{{('/images/PETRO_PEQUEÑO.png') }}" alt="Inicio" width="150" height="80" /></td>
                                <!--td><div class="col col-12 col-sm-6 col-md-6 col-lg-4 petros">
                                        <div class="texto">
                                            <p>{{$petros}}</p>
                                        </div>  
                                    </div>   
                                </td-->

                            
                                </tr>
                                </table>
                                
                                
                                </div>
                            </div>


                            </div>
                        </div>
            </div>     
    </div>  

        <div class="card-body home">
            <div class="row"> 
                <div class="col col-12 col-sm-6 col-md-6 col-lg-4 ventas">
                    <div class="texto">
                         <p>{{$ventas}}</p>
                    </div>   
                </div>                
                <div class="col col-12 col-sm-6 col-md-6 col-lg-4 petros">
                     <div class="texto">
                         <p>{{$petros}}</p>
                    </div>  
                </div>                
                <div class="col col-12 col-sm-6 col-md-6 col-lg-4 productos">
                    <div class="texto">
                        <p>{{$productos}}</p>
                    </div>  
                <div>

            </div>   
            
        </div>

   
</div>  


@endsection