<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
	<title>Llamado de emergencia</title>
	<style>
.correo-recibo {
  width:250px;
}
td {
border:hidden;
}
table {
border: 1px solid black;
}

</style>
</head>
<body>
<div class="row justify-content-md-center">
      
	  <div class="correo-recibo">

		  <div class="card">

				<div class="card-body">	
					
					<div class="table-container">
            			<table id="mytable" class="table table-borderless">
						  <tr><td  align="center" colspan ='4' ><b><h1>Tienda Petro</h1><b></td></tr>
						  <tr><td  align="center" colspan ='4' ><b><h3>Recibo de Compra</h3><b></td></tr>
						  <tr><td colspan ='3'>{{$recibo->venta->created_at}} </td><td>00000{{$recibo->venta->id}} </td></tr> 
						  <tr><td colspan ='4'><hr></td></tr>
						  <tr><td align="center" colspan ='4'>Datos del Consumidor</td></tr>
						  <tr><td colspan ='4'>Nombre: {{$recibo->venta->comprador}}</td></tr>
						  <tr><td colspan ='4'>Cedula: {{$recibo->venta->cedula}} </td></tr>
							
						  <tr><td colspan ='4'>Email: {{$recibo->venta->correo}} </td></tr>
						  <tr><td colspan ='4'><hr></td></tr>
						  <tr>
							<td WIDTH="5%">Art.</td>
							<td WIDTH="50%">Descrip.</td>		
							<td WIDTH="5%">Cant</td>	
							<td WIDTH="40%">C.Unitario</td>
						  </tr>

						  @foreach($recibo->productos as $prod)
						 	<tr>
							<td>{{ $prod->productos->id }}</td>
							<td>{{ $prod->productos->nombre }}</td><td>{{ $prod->cantidad }}</td><td>{{ $prod->montoUnitario }}</td>
								
							
							</tr>
    					  @endforeach
						  <tr><td colspan ='4'><hr></td></tr>
						  <tr><td colspan ='3'>Monto Total {{$recibo->venta->moneda}} </td><td>{{$recibo->venta->montoTotal}} </td></tr>
						  <tr><td colspan ='4'><hr></td></tr>
						</table>
						 
					
					 		</div>	
					 	</div>	
					</div>	
					 
				</div>	
		</div>

</body>
</html>