<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


//Route::resource('productos', 'ProductosController');

Route::prefix('productos')->group(function () {
    Route::get('/', 'ProductosController@index')->name('productos_index');
    Route::get('/create', 'ProductosController@create')->name('productos_create');
    Route::post('/store', 'ProductosController@store')->name('productos_store');
    Route::delete('/destroy/{id}', 'ProductosController@destroy')->name('producto_destroy');
    Route::get('/edit/{id}', 'ProductosController@edit')->name('producto_edit');
    Route::post('/update/{id}', 'ProductosController@update')->name('producto_update');
});

Route::prefix('tienda')->group(function () {
    Route::get('/', 'TiendaController@index')->name('tienda_index');
    Route::get('/create', 'TiendaController@create')->name('tienda_create');
    Route::post('/store', 'TiendaController@store')->name('tienda_store');
    Route::delete('/destroy/{id}', 'TiendaController@destroy')->name('tienda_destroy');
    Route::get('/edit/{id}', 'TiendaController@edit')->name('tienda_edit');
    Route::post('/update/{id}', 'TiendaController@update')->name('tienda_update');
});

Route::prefix('almacen')->group(function () {
    Route::get('/', 'AlmacenController@index')->name('almacen_index');
    Route::get('/create', 'AlmacenController@create')->name('almacen_create');
    Route::post('/store', 'AlmacenController@store')->name('almacen_store');
    Route::delete('/destroy/{id}', 'AlmacenController@destroy')->name('almacen_destroy');
    Route::get('/edit/{id}', 'AlmacenController@edit')->name('almacen_edit');
    Route::post('/update/{id}', 'AlmacenController@update')->name('almacen_update');
   
//agregar almacen a tienda
    Route::get('/agregar/{id}', 'AlmacenController@agregarAlmacenTienda')->name('almacen_agregar');
    Route::post('/update-agregar-almace/{id}', 'AlmacenController@updateAgregarAlmace')->name('almacen_agregar_update');
   
});
Route::prefix('ventas')->group(function () {
    Route::get('/', 'VentasController@index')->name('ventas_index');
    Route::get('/new', 'VentasController@new')->name('ventas_new');
    Route::post('/create', 'VentasController@create')->name('ventas_create');
    Route::get('/ajax-costo-producto', 'VentasController@costoProducto')->name('ajax_ventas_costo_producto');
    Route::get('/ajax-verificar-referencias-cryptos', 'VentasController@verificarReferenciasCryptos')->name('ajax_verificar_referencias_cryptos');
    Route::get('/export', 'VentasController@export')->name('ventas_export');
    Route::get('/recibo/{id}', 'VentasController@recibo')->name('recibo_export');
    Route::get('/ver/{id}', 'VentasController@show')->name('ventas_ver');
});
Route::prefix('obsequios')->group(function () {
    Route::get('/', 'ObsequiosController@index')->name('obsequios_index');
    Route::get('/new', 'ObsequiosController@new')->name('obsequios_new');
    Route::post('/create', 'ObsequiosController@create')->name('obsequios_create');
    Route::get('/ajax-costo-producto', 'ObsequiosController@costoProducto')->name('ajax_ventas_costo_producto');
    Route::get('/ajax-verificar-referencias-cryptos', 'ObsequiosController@verificarReferenciasCryptos')->name('ajax_verificar_referencias_cryptos');
    Route::get('/export', 'ObsequiosController@export')->name('obsequios_export');
});

Route::prefix('billeteras')->group(function () {
    Route::get('/', 'BilleterasController@index')->name('billeteras_index');
    Route::get('/new', 'BilleterasController@create')->name('billeteras_new');
    Route::post('/create', 'BilleterasController@create')->name('billeteras_create');
    Route::post('/store', 'BilleterasController@store')->name('billeteras_store');
    Route::get('/edit/{id}', 'BilleterasController@edit')->name('billeteras_edit');
    Route::post('/update/{id}', 'BilleterasController@update')->name('billeteras_update');
    Route::delete('/destroy/{id}', 'BilleterasController@destroy')->name('billeteras_destroy');
});
Route::prefix('inventario')->group(function () {
    Route::get('/', 'InventarioController@index')->name('inventario_index');
    Route::post('/store', 'InventarioController@store')->name('inventario_store');
    Route::get('/new', 'InventarioController@new')->name('inventario_new');
    Route::get('/create', 'InventarioController@create')->name('inventario_create');
    Route::get('/ajuste/{id}', 'InventarioController@ajuste')->name('inventario_ajuste');
    Route::post('/retirar', 'InventarioController@retirarProductoInventario')->name('inventario_retirar');
    Route::delete('/destroy/{id}', 'InventarioController@destroy')->name('inventario_destroy');
    Route::get('/edit/{id}', 'InventarioController@edit')->name('inventario_edit');
    Route::post('/update/{id}', 'InventarioController@update')->name('inventario_update');
    Route::get('/export', 'InventarioController@export')->name('inventario_export');
    Route::get('/search-almacen', 'InventarioController@getAlmacenes')->name('inventario_search');
    Route::get('/landing', 'InventarioController@landing')->name('landing');
});
Route::prefix('reportes')->group(function () {
    Route::get('/', 'ReportesController@index')->name('reportes_index');
});





Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Email related routes
Route::get('mail/send/{id}', 'MailController@send')->name('mail_recibo');


Route::prefix('admin')->group(function () {
//Abilities and roles
Route::get('/habilidades', 'Admin\AbilitiesController@index')->name('habilidades');
Route::get('/habilidades/create', 'Admin\AbilitiesController@create')->name('admin.habilidades.create');
});


// Administration of users
Route::get('/usuario', 'Admin\UsuarioController@index')->name('admin.usuario.index');
Route::get('/usuario/create', 'Admin\UsuarioController@create')->name('admin.usuario.create');
Route::post('/usuario/store', 'Admin\UsuarioController@store')->name('admin.usuario.store');
Route::get('/usuario/edit/{id}', 'Admin\UsuarioController@edit')->name('admin.usuario.edit');
Route::put('/usuario/update/{id}', 'Admin\UsuarioController@updateUsuario')->name('admin.usuario.update');

Route::get('/cierre', 'InventarioController@cierre')->name('cierre');
Route::get('/conciliacion', 'InventarioController@cierre')->name('conciliacion');


// Administration of roles y abilidades
Route::get('/roles', 'Admin\RolesController@index')->name('admin_roles_index');
Route::get('/roles/edit/{id}', 'Admin\RolesController@edit')->name('admin_roles_edit');
Route::put('/roles/update/{id}', 'Admin\RolesController@update')->name('admin_perfiles_update');
Route::get('/roles/create', 'Admin\RolesController@create')->name('admin_perfiles_create');
Route::post('/roles/store', 'Admin\RolesController@store')->name('admin_roles_store');

Route::get('/abilidades', 'Admin\AbilitiesController@index')->name('admin_habilidades_index');
Route::get('/abilidades/edit/{id}', 'Admin\AbilitiesController@edit')->name('admin_habilidades_edit');
Route::put('/abilidades/update/{id}', 'Admin\AbilitiesController@update')->name('admin_abilidades_update');
Route::post('/abilidades/store', 'Admin\AbilitiesController@store')->name('admin_abilidades_store');